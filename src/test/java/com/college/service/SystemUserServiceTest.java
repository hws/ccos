package com.college.service;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.college.constant.AttachmentConstant;
import com.college.entity.Attachment;
import com.college.entity.Classes;
import com.college.entity.Course;
import com.college.entity.UserClasses;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/applicationContext.xml" })
public class SystemUserServiceTest {

	
	@Autowired
	private SystemUserService systemUserService;

	@Test
	public void test_getClassesByClassCode(){
		String message = "";
		String classCode = "KIMvrTVcZA";
		Classes classes = systemUserService.getClassesByClassCode(classCode);
		if(classes == null){
			message  = "找不到对应的班级";
		}else{
			message  = "junit-test--> 班级名称："+classes.getClassName();
		}
		System.out.println(message);
	}
	
	@Test
	public void test_getUserClassesByClassCode(){
		String message = "";
		String classCode = "eCQ6i1gdG8";
		UserClasses userClasses = systemUserService.getUserClassesByClassCode(classCode);
		if(userClasses == null){
			message  = "找不到对应的班级";
		}else{
			message  = "junit-test--> 班级名称："+userClasses.getClassId();
		}
		System.out.println(message);
	}
	
	
	@Test
	public void test_joinClasses(){
		String message = "";
		//UserClasses userClasses = new UserClasses("6fd69e89-6d18-4177-b0bb-07efb94ab846", "b34ace94-8c16-4583-9c36-190f8faf80a5","汉语言文学","2323131232133adfdfdf");
		UserClasses userClasses = new UserClasses();
		userClasses.setClassId("6fd69e89-6d18-4177-b0bb-07efb94ab846");
		userClasses.setUserId("b34ace94-8c16-4583-9c36-190f8faf80a5");
		int i  = systemUserService.joinClasses(userClasses);
		if(i <= 0){
			message  = "找不到对应的班级";
		}else{
			message  = "junit-test--> 班级名称："+userClasses.getClassId();
		}
		System.out.println(message);
	}
	
	@Test
	public void test_initJobLineDetailData(){
		String message = "";
		
		 systemUserService.initJobLineDetailData("f95a97bc-4004-4300-bd1a-8f28a081ed64","568daae1-85d8-4f95-a248-3173687b0e4a");
		
	}
	
	@Test
	public void test_getJobLineAttachmentList(){
		
		 List<Attachment> attachmentList = systemUserService.getJobLineAttachmentList("3b4bf3a6-6192-11e5-80ec-3144c40ae251",AttachmentConstant.Kind.jobOrderLineFile);
		 for(Attachment attachment:attachmentList){
				attachment.setSize(Math.round(attachment.getSize() / 1024));
				System.out.println(attachment.getSize());
			}
	}

	@Test
	public void test_getCourseByClassIds(){
		List<String> idsList = new ArrayList<String>();
		idsList.add("b34ace94-8c16-4583-9c36-190f8faf80a5");
		String userId = "a6ca2381-1578-41ac-b826-b7c6691ae577";
		List<Course> courseList =  systemUserService.getCourseByClassIds(idsList,userId);
		
	}
	
	
}
