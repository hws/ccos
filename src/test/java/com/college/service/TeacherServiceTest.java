package com.college.service;

import java.util.List;
import java.util.UUID;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.college.entity.Attachment;
import com.college.entity.JobLine;
import com.college.entity.JobLineSummaryInfo;
import com.college.entity.UserClasses;
import com.college.entity.vo.ClassCourseVo;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/applicationContext.xml" })
public class TeacherServiceTest {

	
	@Autowired
	private TeacherService teacherService;

	@Test
	public void test_joinClasses(){
		UserClasses userClasses = new UserClasses("6fd69e89-6d18-4177-b0bb-07efb94ab846", "b34ace94-8c16-4583-9c36-190f8faf80a5","汉语言文学",UUID.randomUUID().toString());
		int i = teacherService.joinClasses(userClasses);
		System.out.println("老师成功加入班级："+i);
	}
	
	@Test
	public void test_getUserClassesCourseList(){
		//UserClasses userClasses = new UserClasses("6fd69e89-6d18-4177-b0bb-07efb94ab846", "b34ace94-8c16-4583-9c36-190f8faf80a5","汉语言文学");
		List<UserClasses> userClasses1 = teacherService.getUserClassesCourseList("6fd69e89-6d18-4177-b0bb-07efb94ab846");
		System.out.println("老师成功加入班级："+userClasses1.get(0).getCreatedTime());
	}
	
	@Test
	public void test_addCourse4Classes(){
		UserClasses userClasses = new UserClasses("6fd69e89-6d18-4177-b0bb-07efb94ab846", "b34ace94-8c16-4583-9c36-190f8faf80a5","汉语言文学","2323131232133adfdfdf");
		int num = teacherService.addCourse4Classes(userClasses);
		System.out.println("老师成功添加课程："+num);
	}
	
	@Test
	public void test_queryJobLinesSummaryInfo(){
		String jobOrderId = "4f77031e-6a21-4633-8be2-3ce74f491453";
		JobLineSummaryInfo num = teacherService.queryJobLinesSummaryInfo(jobOrderId);
	}
	
	@Test
	public void test_updateStuJobLine(){
		JobLine jobLine = new JobLine();
		jobLine.setScore(80d);
		jobLine.setComment("good");
		jobLine.setJobLineId("3b4bf3a6-6192-11e5-80ec-3144c40ae251");
		int num = teacherService.updateStuJobLine(jobLine);
	}
	
	/*@Test
	public void test_getJobLineAttachmentList(){
		String jobOrderId= "4f77031e-6a21-4633-8be2-3ce74f491453";
		List<Attachment> attachmentList = teacherService.getJobLineAttachmentList(jobOrderId);
	}*/
	
	@Test
	public void test_getClassCourseInfoByJobOrdeId(){
		String jobOrderId= "4f77031e-6a21-4633-8be2-3ce74f491453";
		ClassCourseVo classCourseVo = teacherService.getClassCourseInfoByJobOrdeId(jobOrderId);
	}
	
	
	
}
