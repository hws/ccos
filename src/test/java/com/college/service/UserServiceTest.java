package com.college.service;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.college.constant.UserConstant;
import com.college.entity.User;
import com.college.exception.AuthException;
import com.college.util.AuthUtils;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/applicationContext.xml" })
public class UserServiceTest {

	@Autowired
	private UserService userService;

	@Test
	public void test_UserRegister() throws AuthException {
		User user = new User();
		user.setUserId("fa2568f5-7112-46b6-9ad6-66802531efb6");
		user.setEmail("123502@qq.com");
		//user.setGender(0);
		user.setPassword(AuthUtils.getPassword("123", "123502@qq.com"));
		user.setRealName("林老师");
		user.setRoleType(UserConstant.RoleType.STUDENTADMIN);
		int result = userService.userRegister(user);
	}
	
	@Test
	public void test_findUserByEmail() throws AuthException {
		User user = new User();
		user.setUserId("fa2568f5-7112-46b6-9ad6-66802531efb6");
		user.setEmail("12350212@qq.com");
		//user.setGender(0);
		user.setPassword(AuthUtils.getPassword("123", "123502@qq.com"));
		user.setRealName("林老师");
		user.setRoleType(UserConstant.RoleType.ADMIN);
		User existUser = userService.findUserByEmail(user.getEmail());
		System.out.println(user.getEmail());
	}
	
	
	
	

	/*@Test
	public void testGetUserPage() {
		assertEquals(4, userService.getUserPage(1).getList().size());
	}

	@Test
	public void testDeleteUserById() {
		assertEquals(1, userService.deleteUserById(2));
	}
*/
}
