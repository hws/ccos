package com.college.service;

import java.util.List;
import java.util.UUID;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.college.entity.Classes;
import com.college.entity.College;
import com.college.entity.Province;
import com.college.entity.University;
import com.college.exception.AuthException;
import com.college.util.GenerateCode;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/applicationContext.xml" })
public class StuAdminServiceTest {

	
	@Autowired
	private StuAdminService stuAdminService;

	/**
	 *查找班级
	 * @throws bob
	 */
	@Test
	public void test_findClassByCode(){
		String classCode= "nP3GQ6Z3tS";
		Classes classes = stuAdminService.findClassByCode(classCode);
		
		System.out.println(classes.getClassId());
	}
	
	
	/**
	 *查找班级
	 * @throws bob
	 */
	@Test
	public void test_findClassByCreatorId(){
		String creatorId= "b309577a-1354-45cd-8894-3360fe6631c4";
		Classes classes = stuAdminService.findClassByCreatorId(creatorId);
		
		System.out.println(classes.getClassCode());
	}
	
	/**
	 *创建班级
	 * @throws bob
	 */
	@Test
	public void test_createClasses(){
		Classes classes = new Classes();
		classes.setClassId(UUID.randomUUID().toString());
		//(#{classId},#{className},#{subjectName},#{creatorId},#{grade},#{universityId},#{provinceId},#{collegeId})
		classes.setClassName("123123123");
		classes.setSubjectName("计算机科学与技术");
		classes.setGrade("2015");
		classes.setUniversityId("1001");
		classes.setProvinceId("1");
		classes.setCollegeId("1001001");
		classes.setCreatorId("1bb34e4b-c4f4-495e-b2db-206eb86a7122");
		classes.setClassNumber("03");
		classes.setClassCode(GenerateCode.getRamdonCode());
		
		int i  = stuAdminService.createClasses(classes);
		
		System.out.println(classes.getClassId());
	}
	
	
	

	
	
	
}
