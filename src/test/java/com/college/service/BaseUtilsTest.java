
package com.college.service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import com.college.entity.HomeWorkOrder;
import com.college.exception.AuthException;
import com.college.util.BaseUtils;
import com.mysql.jdbc.log.Log;

/**
 * 基本的工具类
 * 
 * @author bob
 * 
 */
public class BaseUtilsTest {

	/**
	 * 获取list对象里的id值拼凑字符串("123","333")
	 * @param list
	 * @return
	 * @throws AuthException
	 */

	public String getIds()
			throws AuthException {
		
		List<HomeWorkOrder> list1 = new ArrayList<HomeWorkOrder>();
		HomeWorkOrder order1 = new HomeWorkOrder();
		order1.setJobOrderId(UUID.randomUUID().toString());
		list1.add(order1);
		
		HomeWorkOrder order2 = new HomeWorkOrder();
		order2.setJobOrderId(UUID.randomUUID().toString());
		list1.add(order2);
		
		List<HomeWorkOrder> list = new ArrayList<HomeWorkOrder>();
		list = list1;
		if(list.size() > 0 && list.get(0) instanceof HomeWorkOrder){
			HomeWorkOrder order;
			StringBuffer sb = new StringBuffer();
			for(int i = 0, l = list.size(); i< l ; i++){
				order = (HomeWorkOrder) list.get(i);
				if(i == (l-1)){
					sb.append("\""+order.getJobOrderId()+"\"");
				}else{
					sb.append("\""+order.getJobOrderId()+"\""+",");
				}
				
			}
			return sb.toString();
		}
		return "";
		
	}
	
	@Test
	public void test_getIds(){
		String testStr = "";
		try {
			 testStr = getIds();
		} catch (AuthException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println(testStr);
	}
	
	
	@Test
	public void test_getIdList() throws AuthException{
		List<HomeWorkOrder> list1 = new ArrayList<HomeWorkOrder>();
		HomeWorkOrder order1 = new HomeWorkOrder();
		order1.setJobOrderId(UUID.randomUUID().toString());
		list1.add(order1);
		
		HomeWorkOrder order2 = new HomeWorkOrder();
		order2.setJobOrderId(UUID.randomUUID().toString());
		list1.add(order2);
		
		List<HomeWorkOrder> releasedHomeWorkList = new ArrayList<HomeWorkOrder>();
		releasedHomeWorkList = list1;
		
		List<String> list = BaseUtils.getIds(releasedHomeWorkList);
		System.out.println("size:"+list.size());
	}
	
	

	
}
