package com.college.service;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.college.constant.SubmitStatusConstant;
import com.college.entity.HomeWorkOrder;
import com.college.entity.JobLine;
import com.college.entity.UserClasses;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/applicationContext.xml" })
public class CommonStudentServiceTest {

	
	@Autowired
	private CommonStudentService CommonStudentService;

	@Test
	public void test_getReleasedHomeWorkList(){
		String message = "";
		String classId = "568daae1-85d8-4f95-a248-3173687b0e4a";
		String userId = "38dddada-9d26-4db5-90ca-9dd1232ead62";
		List<HomeWorkOrder> orderList = CommonStudentService.getReleasedHomeWorkList(classId,userId);
	}
	
	
	@Test
	public void test_updateJobLineStatusById(){
		String message = "";
		JobLine jobLine = new JobLine();
		jobLine.setJobLineId("d17417e4-5f80-11e5-98d9-080027000c42");
		jobLine.setSubmitStatus(2);
		jobLine.setUpdateDate(new Date());
		
		int i = CommonStudentService.updateJobLineStatusById(jobLine);
		if(i > 0){
			message = "有数据：";
		}else{
			message = "没有数据：";
		}
		System.out.println(message);
	}
	
	
	
}
