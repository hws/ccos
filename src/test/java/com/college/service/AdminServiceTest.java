package com.college.service;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.college.entity.College;
import com.college.entity.Province;
import com.college.entity.University;
import com.college.exception.AuthException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/applicationContext.xml" })
public class AdminServiceTest {

	
	@Autowired
	private AdminService adminService;

	/**
	 * 查找所有省份信息
	 * @throws bob
	 */
	@Test
	public void test_getProviceList(){
		List<Province> proList = adminService.getProviceList();
		
		System.out.println(proList.get(0).getProvince_id());
	}
	
	/**
	 * <!-- 根据省份id查找大学信息 -->
	 * @throws bob
	 */
	@Test
	public void test_getUniversityList(){
		List<University> universityList = adminService.getUniversityList("1");
		
		System.out.println(universityList.get(0).getUniversity_id());
	}
	
	
	/**
	 *  根据大学id查找院校信息 
	 * @throws AuthException
	 */
	@Test
	public void test_getCollegeList() {
		
		List<College> collegeList = adminService.getCollegeList("1001");
		
		System.out.println(collegeList.get(0).getCollege());
	}
	
	
	
}
