package com.college.service;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.college.constant.JobOrderConstant;
import com.college.entity.Attachment;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/applicationContext.xml" })
public class JobOrderServiceTest {

	
	@Autowired
	private JobOrderService jobOrderService;

	@Test
	public void test_updateJobOrderStatusById(){
		String message ="Fail to update!";
		int i = jobOrderService.updateJobOrderStatusById("eb167251-ced2-42ce-a18c-986b811bab97", new JobOrderConstant().convertToIntValue(JobOrderConstant.Status.trash));
		if(i > 0){
			message = "success to update!";
		}
		System.out.println(message);
	}
	
	@Test
	public void test_getAttachmentListByJorOrderIds(){
		String message ="Fail to update!";
		String ids = "4f77031e-6a21-4633-8be2-3ce74f491453','eb4a6ec9-07bf-4cb7-a7da-2539651bc703";//"f95a97bc-4004-4300-bd1a-8f28a081ed64"
		
		List<String> idList = new ArrayList<String>();
		idList.add(0,"4f77031e-6a21-4633-8be2-3ce74f491453");
		idList.add(1,"eb4a6ec9-07bf-4cb7-a7da-2539651bc703");
		List<Attachment> attachmentList = jobOrderService.getAttachmentListByJorOrderIds(idList);
		if(attachmentList.size() > 0){
			message = "success to update!"+attachmentList.get(0).getName();
		}
		System.out.println(message);
	}
	
	

}
