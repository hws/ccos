package com.college.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.college.dao.CommonStudentDao;
import com.college.entity.HomeWorkOrder;
import com.college.entity.JobLine;



/**
 * 
 * 
 * @author bob
 * 
 */
@Service
public class CommonStudentService {

	@Autowired
	private CommonStudentDao commonStudentDao;


	/**
	 * 查询学生所在班级所有作业信息
	 * @param classId
	 * @param userId 
	 * @return
	 */
	public List<HomeWorkOrder> getReleasedHomeWorkList(String classId, String userId) {
		return commonStudentDao.getReleasedHomeWorkList(classId,userId);
	}



	public int updateJobLineStatusById(JobLine jobLine) {
		return commonStudentDao.updateJobLineStatusById(jobLine);
		
	}
	

	
}
