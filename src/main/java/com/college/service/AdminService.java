package com.college.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.college.dao.AdminDao;
import com.college.entity.College;
import com.college.entity.Province;
import com.college.entity.University;



/**
 * 系统管理员，维护后台基础数据
 * 
 * @author bob
 * 
 */
@Service
public class AdminService {

	@Autowired
	private AdminDao adminDao;
	
	/**
	 * 获取所有省份信息
	 * @return
	 */
	public List<Province> getProviceList(){
		return adminDao.getProviceList();
	}
	
	/**
	 * 获取所有大学信息
	 * @return
	 */
	public List<University> getUniversityList(String provinceId){
		return adminDao.getUniversityList(provinceId);
	}
	
	/**
	 * 获取所有院校信息
	 * @return
	 */
	public List<College> getCollegeList(String universityId){
		return adminDao.getCollegeList(universityId);
	}
	
	
	

	
}
