package com.college.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.college.dao.JobOrderDao;
import com.college.entity.Attachment;



/**
 * 
 * 
 * @author bob
 * 
 */
@Service
public class JobOrderService {

	@Autowired
	private JobOrderDao jobOrderDao;

	
	public int updateJobOrderStatusById(String jobOrderId,int status) {
		return jobOrderDao.updateJobOrderStatusById(jobOrderId,status);
	}
	
	public List<Attachment> getAttachmentListByJorOrderIds(List<String> jobOrderIdList){
		return jobOrderDao.getAttachmentListByJorOrderIds(jobOrderIdList);
		
	}


	
}
