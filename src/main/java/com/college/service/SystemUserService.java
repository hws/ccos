package com.college.service;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.college.constant.AttachmentConstant;
import com.college.constant.AttachmentConstant.Kind;
import com.college.constant.SystemConstant;
import com.college.dao.SystemUserDao;
import com.college.entity.Attachment;
import com.college.entity.Classes;
import com.college.entity.Course;
import com.college.entity.UserClasses;
import com.college.exception.UploadException;
import com.college.util.UploadUtils;



/**
 * 用户的基本数据加载
 * 
 * @author bob
 * 
 */
@Service
public class SystemUserService {

	@Autowired
	private SystemUserDao systemUserDao;
	
	/**
	 * 获取所有省份信息
	 * @return
	 */
	public List<Classes> getMyClassesList(String userId){
		return systemUserDao.getMyClassesList(userId);
	}

	
	/**
	 * 查询所属班级的课程
	 * @param classId
	 * @return
	 */
	public List<Course> getCourseListByClassId(String classId) {
		return systemUserDao.getCourseListByClassId(classId);
	}

	
	/**
	 * 根据班级代码查询班级
	 * @param classCode
	 * @return
	 */
	public Classes getClassesByClassCode(String classCode) {
		return systemUserDao.getClassesByClassCode(classCode);
	}
	
	

	/**
	 * 用户-班级
	 * @param classCode
	 * @return
	 */
	public UserClasses getUserClassesByClassCode(String classCode) {
		return systemUserDao.getUserClassesByClassCode(classCode);
	}
	

	/**
	 * 用户-班级
	 * @param classCode
	 * @return
	 */
	public UserClasses getUserClassesByUserId(String userId) {
		return systemUserDao.getUserClassesByUserId(userId);
	}
	
	
	public int joinClasses(UserClasses userClasses) {
		return systemUserDao.joinClasses(userClasses);
	}
	
	
	/*
	 * 上传附件
	 */
	
	public Attachment addUploadFile(MultipartFile multipartFile,
			String fileName, String foreignKeyId ,AttachmentConstant.Kind kind,String userId
			) throws IllegalStateException,
			IOException, UploadException {
		AttachmentConstant.Type type = AttachmentConstant.Type.photo;
		if (UploadUtils.isFileType(fileName, UploadUtils.PHOTO_TYPE)) {
			type = AttachmentConstant.Type.photo;
		} else if (UploadUtils.isFileType(fileName, UploadUtils.FILE_TYPE)) {
			type = AttachmentConstant.Type.file;
		} else {
			throw new UploadException("文件类型有误");
		}
		Date now = new Date();
		String uploadPath = UploadUtils.getUploadPath(fileName, now.getTime());
		Attachment attachment = new Attachment();
		//attachment.setKindId(kindId);
		attachment.setDescription("");
		attachment.setName(fileName);
		attachment.setPath(uploadPath);
		attachment.setType(type);
		//attachment.setLink("javascript:void(0);");
		attachment.setSize(multipartFile.getSize());
		attachment.setKind(kind);
		attachment.setUserId(userId);
		//attachment.setStatus(status);
		attachment.setForeignKeyId(foreignKeyId);
		systemUserDao.addAttachment(attachment);
		multipartFile.transferTo(new java.io.File(
				SystemConstant.COLLEGE_CCOS_ROOT + uploadPath));
		return attachment;
	}


	public void initJobLineDetailData(String jobOrderId, String classId) {
		systemUserDao.initJobLineDetailData(jobOrderId,classId);
		
	}


	public List<Attachment> getJobLineAttachmentList(String jobLineId,
			Kind jobOrderLineFile) {
		return systemUserDao.getJobLineAttachmentList(jobLineId,jobOrderLineFile);
	}


	/**
	 * 根据id获取attachment
	 * @param attachmentId
	 * @return
	 */
	public Attachment getAttachmentById(long attachmentId) {
		return systemUserDao.getAttachmentById(attachmentId);
	}


	/**
	 * 删除附件（1.数据库记录attachment table，2.物理删除文件）
	 * @param attachmentId
	 * @param path
	 */
	public void deleteAttachment(long attachmentId, String path) {
		systemUserDao.deleteAttachment(attachmentId);
		UploadUtils.deleteFile(path);
		
	}
	
	public List<Course> getCourseByClassIds(List<String> classIds,String userId) {
		return systemUserDao.getCourseByClassIds(classIds,userId);
	}
	
	

	
}
