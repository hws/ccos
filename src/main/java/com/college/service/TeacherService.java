package com.college.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.college.constant.PageConstant;
import com.college.dao.TeacherDao;
import com.college.entity.Attachment;
import com.college.entity.HomeWorkOrder;
import com.college.entity.JobLine;
import com.college.entity.JobLineSummaryInfo;
import com.college.entity.UserClasses;
import com.college.entity.vo.AttachmentVo;
import com.college.entity.vo.ClassCourseVo;
import com.college.entity.vo.JobLineDetailVo;
import com.college.entity.vo.PageVo;
import com.github.miemiedev.mybatis.paginator.domain.PageBounds;



/**
 * 
 * 
 * @author bob
 * 
 */
@Service
public class TeacherService {

	@Autowired
	private TeacherDao teacherDao;

	public int joinClasses(UserClasses userClasses) {
		return teacherDao.joinClasses(userClasses);
	}
	
	
	public List<UserClasses> getUserClassesCourseList(String userId) {
		return teacherDao.getUserClassesCourseList(userId);
	}

	/**
	 * 老师加入班级时，初始化课程表信息
	 * @param userClasses
	 */
	public int addCourse4Classes(UserClasses userClasses) {
		return teacherDao.addCourse4Classes(userClasses);
		
	}


	/**
	 * 老师发布作业信息
	 * @return
	 */
	public int releaseHomeWork(HomeWorkOrder homeWorkOrder) {
		return teacherDao.releaseHomeWork(homeWorkOrder);
	}


	/**
	 * 查询老师发布的作业信息
	 * @param userId
	 * @param homeWorkOrder 
	 * @return
	 */
	public PageVo<HomeWorkOrder> getReleasedHomeWorkListPage(String userId, HomeWorkOrder homeWorkOrder,int currentPageNum) {
		PageVo<HomeWorkOrder> pageVo = new PageVo<HomeWorkOrder>(currentPageNum);
		pageVo.setLimit(PageConstant.limit);
		List<HomeWorkOrder> HomeWorkOrderList = teacherDao.getReleasedHomeWorkListPage(userId,homeWorkOrder, new PageBounds(currentPageNum,PageConstant.limit,true));
		int count = teacherDao.getReleasedHomeWorkListCount(userId,homeWorkOrder);
		pageVo.setList(HomeWorkOrderList);
		pageVo.setCount(count);
		return pageVo;
	}


	public List<JobLineDetailVo> queryStuJobLinesDetail(String jobOrderId) {
		return teacherDao.queryStuJobLinesDetailList(jobOrderId);
	}


	public JobLineSummaryInfo queryJobLinesSummaryInfo(String jobOrderId) {
		return teacherDao.queryJobLinesSummaryInfo(jobOrderId);
	}


	public int updateStuJobLine(JobLine jobLine) {
		return teacherDao.updateStuJobLine(jobLine);
		
	}


	/**
	 * 老师获取学生提交作业的附件信息
	 * @param jobOrderId
	 * @return
	 */
	public List<AttachmentVo> getJobLineAttachmentList(String jobOrderId) {
		return teacherDao.getJobLineAttachmentList(jobOrderId);
	}


	/**
	 * 根据jobOrderId查找对应的班级和课程信息
	 * @param jobOrderId
	 * @return
	 */
	public ClassCourseVo getClassCourseInfoByJobOrdeId(String jobOrderId) {
		return teacherDao.getClassCourseInfoByJobOrdeId(jobOrderId);
	}


	

	
}
