package com.college.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.college.dao.StuAdminDao;
import com.college.dao.UserDao;
import com.college.entity.Classes;



/**
 * 学生管理员（班委，具有一定管理权限）
 * 
 * @author bob
 * 
 */
@Service
public class StuAdminService {

	@Autowired
	private StuAdminDao stuAdminDao;

	public Classes findClassByCode(String classCode) {
		// TODO Auto-generated method stub
		return stuAdminDao.findClassByCode(classCode);
	}
	
	public Classes findClassByCreatorId(String creatorId){
		return stuAdminDao.findClassByCreatorId(creatorId);
	}
	
	
	public int createClasses(Classes classes) {
		return stuAdminDao.createClasses(classes);
	}
	

	
}
