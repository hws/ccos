/*
 *	Copyright © 2013 Changsha Shishuo Network Technology Co., Ltd. All rights reserved.
 *	长沙市师说网络科技有限公司 版权所有
 *	http://www.shishuo.com
 */

package com.college.constant;

/**
 * 作业常量（jobOrder） * 
 * @author bob
 * 
 */
public class JobOrderConstant {

	/**
	 * 是否拥护配图
	 * 
	 * @author bob
	 * 
	 */
	public static enum Picture {
		no_exist, exist
	};

	/**
	 * 文件状态
	 * 
	 * @author bob
	 * 
	 */
	public static enum Status {
		/**
		 * 初始化，将被系统自动清理
		 */
		init,
		/**
		 * 隐藏
		 */
		hidden, /**
		 * 垃圾
		 */
		trash,
		/**
		 * 公开的
		 */
		display,
	};

	/**
	 * @author bob 
	 * 
	 */
	public static enum Owner {
		/**
		 * 系统创建
		 */
		system, /**
		 * 应用创建
		 */
		app
	};
	
	public int convertToIntValue(JobOrderConstant.Status status){
		int result = 4;
		if("trash".equalsIgnoreCase(status.toString())){
			result =  5;//trash
		}
		return result;	
	}
}
