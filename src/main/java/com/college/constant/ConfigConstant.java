
package com.college.constant;

import org.springframework.stereotype.Component;

/**
 * 系统配置常量
 * 
 * @author bob
 */
@Component
public class ConfigConstant {

	/**
	 * 默认的模板
	 */
	public static String DEFAUTL_TEMPLATE = "default";

	public static String SYS_SITEDESC = "sys_sitedesc";
	public static String SYS_SITENAME = "sys_sitename";
	public static String SYS_THEME = "sys_theme";
	
	public static String SYS_CAPTCHA ="sys_captcha";
}
