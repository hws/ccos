/*
 *	Copyright © 2013 Changsha Shishuo Network Technology Co., Ltd. All rights reserved.
 *	长沙市师说网络科技有限公司 版权所有
 *	http://www.shishuo.com
 */

package com.college.constant;

/**
 * 作业常量（jobOrder） * 
 * @author bob
 * 
 */
public class SubmitStatusConstant {

	
	public static enum SubmitStatus {
		/**
		 * 已提交状态
		 */
		submitted,
		needSubmit
		
	};

	
	public int convertToIntValue(SubmitStatus status){
		int result = 4;
		if("submitted".equalsIgnoreCase(status.toString())){
			result =  1;//trash
		}else if("needSubmit".equalsIgnoreCase(status.toString())){
			result = 2;
		}
		return result;	
	}
}
