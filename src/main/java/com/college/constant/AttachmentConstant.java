package com.college.constant;

/**
 * 附件
 * 
 * @author bob
 * 
 */
public class AttachmentConstant {

	/**
	 * 类型<br>
	 * photo：照片<br>
	 * file：文件<br>
	 * 
	 * @author bob
	 * 
	 */
	public static enum Type {
		/**
		 * 相册
		 */
		photo, /**
		 * 文件
		 */
		file
	}

	/**
	 * 附件种类（老师上传的对应jobOrder，学生上传的jobOrderLine）
	 * 
	 * @author bob
	 * 
	 */
	public static enum Kind {
		jobOrderFile, jobOrderLineFile
	}

	public static enum Status {
		/**
		 * 隐藏是嵌入文章的
		 */
		hidden, /**
		 * 没有嵌入文章的
		 */
		display
	}
}
