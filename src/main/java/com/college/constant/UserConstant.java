package com.college.constant;

/**
 * @author bob
 * 
 */
public class UserConstant {

	/**
	 * @author bob
	 * 系统后台管理员，老师，学生，学生管理员
	 */
	public static enum RoleType {
		ADMIN,TEACHER,STUDENT,STUDENTADMIN
	};
	
	public static enum Gender {
		MALE,FEMALE
	};
	
	

}
