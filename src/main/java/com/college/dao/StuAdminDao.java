
package com.college.dao;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.college.entity.Classes;


/**
 * 用户服务
 * 
 * @author bob
 * 
 */

@Repository
public interface StuAdminDao {

	
	
	/**
	 * 根据班级代码查找班级
	 * @param code
	 * @return
	 */
	public Classes findClassByCode(@Param("classCode") String classCode);

	/**
	 * 创建班级
	 * 
	 * @param User
	 * @return Integer
	 */
	public int createClasses(Classes classes);

	
	/**
	 * 根据创建者id查找班级
	 * @param creatorId
	 * @return
	 */
	public Classes findClassByCreatorId(@Param("creatorId") String creatorId);



}
