
package com.college.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.college.entity.HomeWorkOrder;
import com.college.entity.JobLine;


/**
 * 普通学生
 * 
 * @author bob
 * 
 */

@Repository
public interface CommonStudentDao {


	List<HomeWorkOrder> getReleasedHomeWorkList(@Param("classId")String classId,@Param("userId") String userId);

	int updateJobLineStatusById(JobLine jobLine);

	


}
