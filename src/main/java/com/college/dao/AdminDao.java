
package com.college.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.college.entity.Classes;
import com.college.entity.College;
import com.college.entity.Province;
import com.college.entity.University;


/**
 * 用户服务
 * 
 * @author bob
 * 
 */

@Repository
public interface AdminDao {

	/**
	 * 查询所有省份信息
	 * 
	 * @param User
	 * @return Integer
	 */
	public int createClass(Classes classes);

	public List<Province> getProviceList();

	public List<College> getCollegeList(@Param("universityId") String universityId);

	public List<University> getUniversityList(@Param("provinceId") String provinceId);



}
