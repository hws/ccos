
package com.college.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.college.constant.AttachmentConstant.Kind;
import com.college.entity.Attachment;
import com.college.entity.Classes;
import com.college.entity.Course;
import com.college.entity.UserClasses;


/**
 * 用户服务
 * 
 * @author bob
 * 
 */

@Repository
public interface SystemUserDao {

	/**
	 * 查询老师任教的班级
	 * @return
	 */
	public List<Classes> getMyClassesList(@Param("userId") String userId);

	/**
	 * 根据班级id查询对应的课程名称
	 * @param classId
	 * @return
	 */
	public List<Course> getCourseListByClassId(@Param("classId")String classId);

	/**
	 * 根据班级代码查询班级（班级代码唯一，班级代码和班级是一对一的关系）
	 * @param classCode
	 * @return
	 */
	public Classes getClassesByClassCode(String classCode);
	
	
	/**
	 * 用户对应所在的班级
	 * @param classCode
	 * @return
	 */
	public UserClasses getUserClassesByClassCode(@Param("classCode") String classCode);

	/**
	 * 用户加入班级
	 * @param userClasses
	 * @return
	 */
	public int joinClasses(UserClasses userClasses);

	public UserClasses getUserClassesByUserId(String userId);
	
	
	/**用户上传附件
	 * @param att
	 * @return
	 */
	public int addAttachment(Attachment att);

	public void initJobLineDetailData(@Param("jobOrderId") String jobOrderId, @Param("classId") String classId);

	/**
	 * 学生获取提交的作业附件信息
	 * @param jobLineId
	 * @param joborderlinefile
	 * @return
	 */
	public List<Attachment> getJobLineAttachmentList(@Param("jobLineId") String jobLineId,
			@Param("jobOrderLineFile") Kind jobOrderLineFile);

	/**
	 * 根据id获取attachment
	 * @param attachmentId
	 * @return
	 */
	public Attachment getAttachmentById(@Param("attachmentId")long attachmentId);

	/**
	 * 删除attachment记录
	 * @param attachmentId
	 * @return
	 */
	public void deleteAttachment(@Param("attachmentId")long attachmentId);

	/**
	 * 根据班级ids查询对应的课程信息
	 * @param classIds
	 * @return
	 */
	public List<Course> getCourseByClassIds(@Param("classIds")List<String> classIds,@Param("userId")String userId);



}
