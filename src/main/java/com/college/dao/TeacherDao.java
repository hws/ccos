
package com.college.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.college.entity.HomeWorkOrder;
import com.college.entity.JobLine;
import com.college.entity.JobLineSummaryInfo;
import com.college.entity.UserClasses;
import com.college.entity.vo.AttachmentVo;
import com.college.entity.vo.ClassCourseVo;
import com.college.entity.vo.JobLineDetailVo;
import com.college.entity.vo.PageVo;
import com.github.miemiedev.mybatis.paginator.domain.PageBounds;



/**
 * 老师服务
 * 
 * @author bob
 * 
 */

@Repository
public interface TeacherDao {

	int  joinClasses(UserClasses userClasses);

	List<UserClasses> getUserClassesCourseList(@Param("userId") String userId);

	int addCourse4Classes(UserClasses userClasses);

	int releaseHomeWork(HomeWorkOrder homeWorkOrder);

	List<HomeWorkOrder> getReleasedHomeWorkListPage(@Param("teacherId")String userId,@Param("homeWorkOrder") HomeWorkOrder homeWorkOrder,PageBounds pageBounds);

	int getReleasedHomeWorkListCount(@Param("teacherId")String userId,@Param("homeWorkOrder") HomeWorkOrder homeWorkOrder);
	
	List<JobLineDetailVo> queryStuJobLinesDetailList(String jobOrderId);

	JobLineSummaryInfo queryJobLinesSummaryInfo(String jobOrderId);

	int updateStuJobLine(JobLine jobLine);

	List<AttachmentVo> getJobLineAttachmentList(@Param("jobOrderId")String jobOrderId);

	ClassCourseVo getClassCourseInfoByJobOrdeId(@Param("jobOrderId")String jobOrderId);

	int getReleasedHomeWorkListCount(String userId,
			HomeWorkOrder homeWorkOrder, PageBounds pageBounds);



	


}
