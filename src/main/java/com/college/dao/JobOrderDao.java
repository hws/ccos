
package com.college.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.college.entity.Attachment;


/**
 * 作业服务
 * 
 * @author bob
 * 
 */

@Repository
public interface JobOrderDao {

	/**
	 * 更新作业的状态信息
	 * @param jobOrderId
	 * @param status
	 * @return
	 */
	public int updateJobOrderStatusById(@Param("jobOrderId") String jobOrderId,
			@Param("orderStatus") int orderStatus);

	/**
	 * 根据jobOrderIds查询作业的附件信息
	 * @param jobOrderIdList
	 * @return
	 */
	public List<Attachment> getAttachmentListByJorOrderIds(@Param("jobOrderIds") List<String> jobOrderIdList);


	


}
