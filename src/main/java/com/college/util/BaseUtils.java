
package com.college.util;

import java.util.ArrayList;
import java.util.List;

import com.college.entity.HomeWorkOrder;
import com.college.exception.AuthException;

/**
 * 基本的工具类
 * 
 * @author bob
 * 
 */
public class BaseUtils {

	/**
	 * 获取list对象里的id值拼凑字符串("123","333")
	 * @param releasedHomeWorkList
	 * @return
	 * @throws AuthException
	 */
	public static List<String> getIds(List<HomeWorkOrder> releasedHomeWorkList)
			throws AuthException {
		List<String> idList = new ArrayList<String>();
		if(releasedHomeWorkList == null){
			return idList;
		}else if(releasedHomeWorkList.size() > 0 && releasedHomeWorkList.get(0) instanceof HomeWorkOrder){
			for(int i = 0, l = releasedHomeWorkList.size(); i< l ; i++){
				
				idList.add(i, releasedHomeWorkList.get(i).getJobOrderId());
				
			}
			return idList;
		}
		return idList;
		
	}

	
}
