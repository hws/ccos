/*package com.college.util;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.college.service.UserService;

public class CheckUserEmailServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;
	

		public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	        doGet(request, response);
	    }

	    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

			ApplicationContext ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(request.getSession().getServletContext());
			UserService userService = ctx.getBean(UserService.class);
			String checkType = request.getParameter("checkType");
			
			
			
	    	String inputValue = request.getParameter("inputValue");//邮箱或者验证码的值
	    	JSONObject json = new JSONObject();
	    	Boolean available = false;
			if(StringUtils.isNotBlank(inputValue) && "emailType".equals(checkType)){
				int result = userService.findUserByEmail(inputValue);
				//int result = 1;
				 available = result > 0 ? false : true;
			}
			
			if(StringUtils.isNotBlank(inputValue) && "validatorCode".equals(checkType)){
				Object sessionCode = request.getSession().getAttribute("checkCode");
				available = inputValue.equals(sessionCode.toString()) ? true : false;
			}
			json.put("valid",available);
			PrintWriter out = response.getWriter();
			out.print(json);

	    }

	 
}
*/