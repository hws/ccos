package com.college.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class ZipUtils {
	public static void main( String[] args ) {
        try {
         String[] strs = new String[5];
         strs[0]="E:/devWorkspace/ccos/src/test/resources/1.jpg";
         strs[1]="E:/devWorkspace/ccos/src/test/resources/2.gif";
        /* strs[2]="E:/devWorkspace/ccos/src/test/resources/3.gif";
         strs[3]="E:/devWorkspace/ccos/src/test/resources/4.txt";
         strs[4]="E:/devWorkspace/ccos/src/test/resources/5.docx";*/
         String[] strs2 = new String[5];
         
         strs2[0]="E:/devWorkspace/ccos/src/test/resources/1-copy1.jpg";
         strs2[1]="E:/devWorkspace/ccos/src/test/resources/2-copy2.gif";
         //文件的列表 和 将要打成的zip文件的名称
           // writeZip(strs,strs2,"E:/devWorkspace/ccos/src/test/resources/test.zip");
            copyFile(strs[1],strs2[1]);
        } catch ( IOException e ) {
            e.printStackTrace();
        }
		
		String path ="/upload/2015/10/04/1443934083349.jpg";
		String suffix = path.substring(path.lastIndexOf("."),path.length());
		String prefixPath = path.substring(0,path.lastIndexOf("/"));
		System.out.println(prefixPath);
    }
    public static void writeZip(String[] strs,String[] newPaths,String targetPath) throws IOException {
        String[] files = strs;
        OutputStream os = new BufferedOutputStream( new FileOutputStream( targetPath));//targetPath=context+zipName+".zip" 
        ZipOutputStream zos = new ZipOutputStream( os );
        byte[] buf = new byte[8192];
        int len;
        for (int i=0;i<files.length;i++) {  
            File file = new File( files[i] );
            //file.renameTo(new File(newPaths[i]));
            copyFile(files[i],newPaths[i]);
            File copyFile = new File( newPaths[i] );
            if ( !file.isFile() ) continue; 
            ZipEntry ze = new ZipEntry( copyFile.getName() );
            zos.putNextEntry( ze );
            BufferedInputStream bis = new BufferedInputStream( new FileInputStream( file ) );
            while ( ( len = bis.read( buf ) ) > 0 ) {
                zos.write( buf, 0, len );
            }
            zos.closeEntry();
            copyFile.delete();
        }
        zos.closeEntry();
        zos.close();
    }
    
    public static void copyFile(String sourcePath,String targetPath) throws IOException{
    	File targetFile = new File(targetPath);
    	File sourceFile = new File(sourcePath);
    	InputStream is = new FileInputStream(sourceFile);
    	OutputStream os = new  FileOutputStream(targetFile);
    	
    	int temp = 0;
    	 byte[] buf = new byte[8192];
    	try {
			while((temp = is.read(buf)) > 0){
				os.write(buf, 0, temp);
			}
			//targetFile.renameTo(targetFile);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			is.close();
			os.close();
		}
    	
    	
    }
}
