package com.college.action.admin;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.college.action.user.BaseAction;
import com.college.constant.SystemConstant;
import com.college.entity.Classes;
import com.college.entity.Course;
import com.college.entity.User;
import com.college.service.SystemUserService;


@Controller
@RequestMapping("/user/system")
public class SystemUserAction extends BaseAction{
	
	@Autowired
	private SystemUserService systemUserService;
	
	/**
	 * 老师对应的班级（多对多的关系）
	 * @param request
	 * @param classEntity
	 * @return
	 */
	@RequestMapping(value = "queryMyClasses.json", method = RequestMethod.GET)
	@ResponseBody
	public List<Classes> queryMyClasses(HttpServletRequest request, Classes classEntity) {
		
		//获取用户的id
		User user = (User) request.getSession().getAttribute(SystemConstant.SESSION_USER);
		List<Classes> classesList = systemUserService.getMyClassesList(user.getUserId());
		return classesList;
	}
	
	/**
	 * 根据班级id查询对应的课程（多对多的实体关系）
	 * @param request
	 * @param classId
	 * @return
	 */
	@RequestMapping(value = "getCourseListByClassId.json", method = RequestMethod.GET)
	@ResponseBody
	public List<Course> getCourseListByClassId(HttpServletRequest request,@RequestParam(value = "classId") String classId) {
		
		List<Course> courseList = systemUserService.getCourseListByClassId(classId);
		return courseList;
	}
	
	
	
	
	
	
	

}
