
package com.college.action.admin;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.college.action.user.BaseAction;
import com.college.entity.College;
import com.college.entity.Province;
import com.college.entity.University;
import com.college.service.AdminService;

/**
 * @author bob
 * 
 */

@Controller
@RequestMapping("/admin")
public class AdminAction extends BaseAction {

	@Autowired
	private AdminService adminService;
	


	@RequestMapping(value = "getProvinceList.json", method = RequestMethod.GET)
	@ResponseBody
	public List<Province> getProvinceList(HttpServletRequest request) {
		
		List<Province> provinceList = adminService.getProviceList();
		return provinceList;
	}
	
	@RequestMapping(value = "getUniversityList.json", method = RequestMethod.GET)
	@ResponseBody
	public List<University> getUniversityList(HttpServletRequest request,@RequestParam String provinceId) {
		/*String provinceId = request.getParameter("provinceId").split("_")[0];
		String universityId = request.getParameter("universityId").split("_")[0];
		String action = request.getParameter("action");*/
		List<University> universityList = adminService.getUniversityList(provinceId.split("_")[0]);
		return universityList;
	}
	
	@RequestMapping(value = "getCollegeList.json", method = RequestMethod.GET)
	@ResponseBody
	public List<College> getCollegeList(HttpServletRequest request,@RequestParam String universityId) {
		List<College> collegeList = adminService.getCollegeList(universityId.split("_")[0]);
		return collegeList;
	}
	

}
