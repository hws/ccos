package com.college.action.user;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller

public class SpringMVCHelloController {

	@RequestMapping("login.do")
	public String printHelloWorld(Model model) {
		model.addAttribute("message", "Hello World!");

		return "helloWorld";
	}
	
	/*@RequestMapping("login.do")
	public String login(Model model) {
		model.addAttribute("message", "Hello ,welcome to login method!");

		return "helloWorld";
	}*/
	
	@RequestMapping("test.do")
	public String login(Model model) {
		model.addAttribute("message", "Hello ,welcome to test method!");

		return "helloWorld";
	}
}
