
package com.college.action.stuAdmin;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.college.action.user.BaseAction;
import com.college.constant.SystemConstant;
import com.college.entity.Classes;
import com.college.entity.User;
import com.college.service.StuAdminService;
import com.college.util.GenerateCode;

/**
 * @author bob
 * 
 */

@Controller
@RequestMapping("/stuAdmin")
public class StuAdminAction extends BaseAction {

	
	@Autowired
	private StuAdminService stuAdminService;


	@RequestMapping(value = "createClassPage.htm", method = RequestMethod.GET)
	public String createClassPage(HttpServletRequest request, Classes classEntity) {
		return "stuAdmin/pages/createClassesPage";
	}
	
	@RequestMapping(value = "createClassSuccess.htm", method = RequestMethod.GET)
	public String createClassSuccess(HttpServletRequest request,@RequestParam("classCode") String classCode,ModelMap model) {
		model.addAttribute("classCode", classCode);
		return "stuAdmin/pages/createClassSuccess";
	}
	
	@RequestMapping(value = "createClass.htm", method = RequestMethod.POST)
	public ModelAndView createClass(HttpServletRequest request, Classes classEntity) {
		String[] proviceArray = classEntity.getProvinceId().split("_");
		String[] collegeArray = classEntity.getCollegeId().split("_");
		String[] universityArray = classEntity.getUniversityId().split("_");
		String className =   collegeArray[1] +"_" +
				classEntity.getGrade() +"_" + classEntity.getSubjectName() +"_" +classEntity.getClassNumber();
		
		Classes classes;
		String code = null;
		do{
			 code = GenerateCode.getRamdonCode();
			 classes = stuAdminService.findClassByCode(code);
		}while(classes != null);
		
		User sessionUser = (User) request.getSession().getAttribute(SystemConstant.SESSION_USER);
		classEntity.setClassId(UUID.randomUUID().toString());
		classEntity.setCreatorId(sessionUser.getUserId());
		classEntity.setClassName(className);
		classEntity.setProvinceId(proviceArray[0]);
		classEntity.setCollegeId(collegeArray[0]);
		classEntity.setUniversityId(universityArray[0]);
		classEntity.setClassCode(code);
		
		stuAdminService.createClasses(classEntity);
		
		ModelAndView mav = new ModelAndView();
		String viewName = "redirect:createClassSuccess.htm?classCode="+code;
		mav.setViewName(viewName);
		mav.addObject("ClassEntity", classEntity);
		return mav;
		//return "stuAdmin/pages/createClassesPage";
	}
	
	
	@RequestMapping(value = "saveClassCode.htm", method = RequestMethod.GET)
	public void saveClassCode(HttpServletRequest request, HttpServletResponse response) {
		
		String fileName = "作业本-班级代码.txt";
		String content = null;
		//根据班级创建人的id查找到对应的班级代码
		User sessionUser = (User) request.getSession().getAttribute(SystemConstant.SESSION_USER);
		content = "班级代码:"+stuAdminService.findClassByCreatorId(sessionUser.getUserId()).getClassCode();
		
		response.setContentType("application/txt;charset=utf-8");
		try {
			response.setHeader("Content-Disposition", "attachment; filename=\"" +new String(fileName.getBytes("utf-8"),"iso8859-1")+"\"");
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		response.setCharacterEncoding("utf-8");
		BufferedOutputStream bos = null;
		try {
			bos = new BufferedOutputStream(response.getOutputStream());
			//bos.write(new byte []{( byte ) 0xEF ,( byte ) 0xBB ,( byte ) 0xBF }); 
			if(StringUtils.isBlank(content)){
				content = "没能成功获取班级代码，请重试！";
			}
			bos.write(content.getBytes("utf-8"));
			bos.flush();
			bos.close();
			bos = null;
		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			if(bos!=null){
				try {
					bos.close();
				} catch (IOException e) {
				}
			}
		}
	}
	

	

}
