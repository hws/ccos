package com.college.form;

import com.college.entity.User;

public class UserForm extends User{
	
	/**
	 * 验证码（注册）
	 */
	private String captcha;
	
	
	/**
	 * 确认密码
	 */
	private String comfirmPassword;

	public String getCaptcha() {
		return captcha;
	}

	public void setCaptcha(String captcha) {
		this.captcha = captcha;
	}

	public String getComfirmPassword() {
		return comfirmPassword;
	}

	public void setComfirmPassword(String comfirmPassword) {
		this.comfirmPassword = comfirmPassword;
	}
	
	
	
	
}
