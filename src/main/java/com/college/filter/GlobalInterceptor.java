package com.college.filter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import org.apache.log4j.spi.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.college.constant.ConfigConstant;
import com.college.constant.SystemConstant;
import com.college.entity.Admin;
import com.college.entity.User;
import com.college.service.ConfigService;
import com.college.util.HttpUtils;



/**
 * @author bob
 * 
 */
@Component
public class GlobalInterceptor implements HandlerInterceptor {

	@Autowired
	private ConfigService configService;
	private final Logger log = Logger.getLogger(GlobalInterceptor.class);  

	@Override
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {
		//没有加入班级的用户，任何访问跳转至加入班级页面。
		/*User user =  (User) request.getSession().getAttribute(
				SystemConstant.SESSION_USER);
		
		Boolean boo = false;
		if(user == null){
			boo = true;
		}else if(user != null && user.getClassIdList() != null && user.getClassIdList().size() > 0 ){
			boo = true;
		}else if(!request.getRequestURI().contains("joinClassPage.htm")){
			String path = request.getContextPath();
			String basePath = request.getScheme() + "://"
					+ request.getServerName() + ":" + request.getServerPort()
					+ path;
			
			log.info("Interceptor：跳转到加入班级的页面！");  
			String forwardUrl = null;
			if("STUDENT".endsWith(user.getRoleType().toString())){
				forwardUrl = "/user/student/joinClassPage.htm";
			}else if("TEACHER".endsWith(user.getRoleType().toString())){
				forwardUrl = "student/pages/joinClassPage.ftl";
			}else if("STUADMIN".endsWith(user.getRoleType().toString())){
				forwardUrl = "student/pages/joinClassPage.ftl";
			}
		    //request.getRequestDispatcher("/WEB-INF/"+forwardUrl).forward(request, response);  
		    //response.sendRedirect(basePath +forwardUrl);
		    return true;
		}*/
		return true;
	}

	@Override
	public void postHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		if (null == modelAndView) {
			return;
		}
		Admin admin = (Admin) request.getSession().getAttribute(
				SystemConstant.SESSION_ADMIN);
		if (admin == null) {
			modelAndView.addObject("isAdmin", false);
		} else {
			modelAndView.addObject("isAdmin", true);
		}
		// 系统配置参数
		String basePath = HttpUtils.getBasePath(request);
		String contextPath = HttpUtils.getContextPath(request);
		modelAndView.addObject("basePath", basePath);
		modelAndView.addObject("contextPath", contextPath);
		modelAndView.addObject("SYS_SITEDESC",
				configService.getConfigByKey(ConfigConstant.SYS_SITEDESC));
		modelAndView.addObject("SYS_SITENAME",
				configService.getConfigByKey(ConfigConstant.SYS_SITENAME));
		modelAndView.addObject("SYS_TEMPLATE",
				configService.getConfigByKey(ConfigConstant.SYS_THEME));
		modelAndView.addObject("TEMPLATE_PATH", basePath);
		MDC.put("ip", HttpUtils.getIp(request));
	}

	@Override
	public void afterCompletion(HttpServletRequest request,
			HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		// TODO Auto-generated method stub

	}

}
