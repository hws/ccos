package com.college.entity;

import java.util.List;

import com.college.constant.UserConstant;

/**
 * 用户实体
 * 
 * @author bob
 * 
 */

public class User {

	/**
	 * 用户ID
	 */
	private String userId;
	
	/**
	 * 邮箱地址
	 */
	private String email;

	
	/**
	 * 密码
	 */
	private String password;
	
	/**
	 * 用户身份角色
	 */
	private UserConstant.RoleType roleType;

	/**
	 * 用户名(实名)
	 */
	private String realName;
	
	/**
	 * 用户昵称
	 */
	private String nickName;
	
	/**
	 * 手机
	 */
	private String telephone;
	
	
	/**
	 * 性别
	 */
	private String gender;//性别
	
	private List<String> classIdList;



	public List<String> getClassIdList() {
		return classIdList;
	}

	public void setClassIdList(List<String> classIdList) {
		this.classIdList = classIdList;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public UserConstant.RoleType getRoleType() {
		return roleType;
	}

	public void setRoleType(UserConstant.RoleType roleType) {
		this.roleType = roleType;
	}

	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}



}
