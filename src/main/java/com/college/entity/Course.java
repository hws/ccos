package com.college.entity;


/**课程实体类
 * @author bob
 * email:305249280@qq.com
 */
public class Course {
	private String courseId;//课程id
	private String classId;//班级id
	
	private String courseName;//课程名称
	
	private String term;//学期

	public String getCourseId() {
		return courseId;
	}

	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}

	public String getClassId() {
		return classId;
	}

	public void setClassId(String classId) {
		this.classId = classId;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public String getTerm() {
		return term;
	}

	public void setTerm(String term) {
		this.term = term;
	}
	
}
