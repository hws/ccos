package com.college.entity;

/**
 * 大学实体类
 * @author bob
 *
 */

public class University {

	
	private String university_id;
	private String university;
	
	private String province_id;
	public String getProvince_id() {
		return province_id;
	}
	public void setProvince_id(String province_id) {
		this.province_id = province_id;
	}
	public String getUniversity_id() {
		return university_id;
	}
	public void setUniversity_id(String university_id) {
		this.university_id = university_id;
	}
	public String getUniversity() {
		return university;
	}
	public void setUniversity(String university) {
		this.university = university;
	}
	
	
}
