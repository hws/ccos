package com.college.entity.vo;

public class ClassCourseVo {
	private String homeWorkName;
	private String className;
	private String courseName;
	
	
	public ClassCourseVo() {
		super();
	}
	
	public String getHomeWorkName() {
		return homeWorkName;
	}
	public void setHomeWorkName(String homeWorkName) {
		this.homeWorkName = homeWorkName;
	}
	public String getClassName() {
		return className;
	}
	public void setClassName(String className) {
		this.className = className;
	}
	public String getCourseName() {
		return courseName;
	}
	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}
	
	
}
