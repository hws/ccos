package com.college.entity.vo;

import java.util.Date;

public class JobLineDetailVo {
	
	
	private String jobLineId;
	private String jobOrderId;
	private AttachmentVo attachmentVo;
	private double score;
	private int submitStatus;
	private Date updateDate;
	private String realName;
	private String userId;
	private String comment;
	
	public JobLineDetailVo() {
		super();
	}

	public String getJobLineId() {
		return jobLineId;
	}

	public void setJobLineId(String jobLineId) {
		this.jobLineId = jobLineId;
	}

	public String getJobOrderId() {
		return jobOrderId;
	}

	public void setJobOrderId(String jobOrderId) {
		this.jobOrderId = jobOrderId;
	}


	public AttachmentVo getAttachmentVo() {
		return attachmentVo;
	}

	public void setAttachmentVo(AttachmentVo attachmentVo) {
		this.attachmentVo = attachmentVo;
	}

	public double getScore() {
		return score;
	}

	public void setScore(double score) {
		this.score = score;
	}

	public int getSubmitStatus() {
		return submitStatus;
	}

	public void setSubmitStatus(int submitStatus) {
		this.submitStatus = submitStatus;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	
	
}
