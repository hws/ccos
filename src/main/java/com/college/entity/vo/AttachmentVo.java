package com.college.entity.vo;

import com.college.entity.Attachment;

public class AttachmentVo extends Attachment{
	private String stuNumber;
	private String realName;
	
	
	public AttachmentVo() {
		super();
	}
	
	public String getStuNumber() {
		return stuNumber;
	}
	public void setStuNumber(String stuNumber) {
		this.stuNumber = stuNumber;
	}
	public String getRealName() {
		return realName;
	}
	public void setRealName(String realName) {
		this.realName = realName;
	}
	
	
}
