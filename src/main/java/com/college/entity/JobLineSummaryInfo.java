
package com.college.entity;




public class JobLineSummaryInfo{
		private int totalNum;
	    private int submittedCount;
		private int needToSubmitCount;
		
	
		
		public JobLineSummaryInfo() {
			super();
		}



		public JobLineSummaryInfo(int totalNum, int submittedCount,
				int needToSubmitCount) {
			super();
			this.totalNum = totalNum;
			this.submittedCount = submittedCount;
			this.needToSubmitCount = needToSubmitCount;
		}
		
		

		public JobLineSummaryInfo(int totalNum, int submittedCount) {
			super();
			this.totalNum = totalNum;
			this.submittedCount = submittedCount;
		}



		public int getTotalNum() {
			return totalNum;
		}

		public void setTotalNum(int totalNum) {
			this.totalNum = totalNum;
		}

		public int getSubmittedCount() {
			return submittedCount;
		}

		public void setSubmittedCount(int submittedCount) {
			this.submittedCount = submittedCount;
		}

		public int getNeedToSubmitCount() {
			return needToSubmitCount;
		}

		public void setNeedToSubmitCount(int needToSubmitCount) {
			this.needToSubmitCount = needToSubmitCount;
		}
		
		
		
	
		
}