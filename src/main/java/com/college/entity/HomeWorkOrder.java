
package com.college.entity;

import org.springframework.web.multipart.MultipartFile;


public class HomeWorkOrder{
	    private String jobOrderId;
		private String classId;
		private String className;
		private String courseId;
		private String courseName;
		private String teacherId;
		private String homeWorkName;
		
		private String homeWorkDescripton;
		private String homeWorkDocument;
		private String startDate;
		private String endDate;
		
		private String statusCode;
		private String statusName;
		
		private int orderType;
		private int orderStatus;
		
		private MultipartFile file;
		
		private Attachment attachment;
		
		private String jobLineId;
		
		private String submitStatus;
		
		
		
		

		public HomeWorkOrder() {
			super();
		}

		public HomeWorkOrder(String jobOrderId, String classId,
				String courseId, String teacherId, String homeWorkName,
				String homeWorkDescripton, String homeWorkDocument,
				String startDate, String endDate) {
			super();
			this.jobOrderId = jobOrderId;
			this.classId = classId;
			this.courseId = courseId;
			this.teacherId = teacherId;
			this.homeWorkName = homeWorkName;
			this.homeWorkDescripton = homeWorkDescripton;
			this.homeWorkDocument = homeWorkDocument;
			this.startDate = startDate;
			this.endDate = endDate;
		}

		
		public int getOrderStatus() {
			return orderStatus;
		}

		public void setOrderStatus(int orderStatus) {
			this.orderStatus = orderStatus;
		}

		public void setOrderType(int orderType) {
			this.orderType = orderType;
		}

		public String getJobLineId() {
			return jobLineId;
		}

		public void setJobLineId(String jobLineId) {
			this.jobLineId = jobLineId;
		}

		public String getSubmitStatus() {
			return submitStatus;
		}

		public void setSubmitStatus(String submitStatus) {
			this.submitStatus = submitStatus;
		}

		public Attachment getAttachment() {
			return attachment;
		}

		public void setAttachment(Attachment attachment) {
			this.attachment = attachment;
		}

		public MultipartFile getFile() {
			return file;
		}

		public void setFile(MultipartFile file) {
			this.file = file;
		}

		public String getCourseName() {
			return courseName;
		}

		public void setCourseName(String courseName) {
			this.courseName = courseName;
		}

		public String getClassName() {
			return className;
		}

		public void setClassName(String className) {
			this.className = className;
		}

		public String getJobOrderId() {
			return jobOrderId;
		}

		public void setJobOrderId(String jobOrderId) {
			this.jobOrderId = jobOrderId;
		}

		public String getClassId() {
			return classId;
		}

		public void setClassId(String classId) {
			this.classId = classId;
		}

		public String getCourseId() {
			return courseId;
		}

		public void setCourseId(String courseId) {
			this.courseId = courseId;
		}

		public String getTeacherId() {
			return teacherId;
		}

		public void setTeacherId(String teacherId) {
			this.teacherId = teacherId;
		}

		public String getHomeWorkName() {
			return homeWorkName;
		}

		public void setHomeWorkName(String homeWorkName) {
			this.homeWorkName = homeWorkName;
		}

		public String getHomeWorkDescripton() {
			return homeWorkDescripton;
		}

		public void setHomeWorkDescripton(String homeWorkDescripton) {
			this.homeWorkDescripton = homeWorkDescripton;
		}

		public String getHomeWorkDocument() {
			return homeWorkDocument;
		}

		public void setHomeWorkDocument(String homeWorkDocument) {
			this.homeWorkDocument = homeWorkDocument;
		}

		public String getStartDate() {
			return startDate;
		}

		public void setStartDate(String startDate) {
			this.startDate = startDate;
		}

		public String getEndDate() {
			return endDate;
		}

		public void setEndDate(String endDate) {
			this.endDate = endDate;
		}

		public String getStatusCode() {
			return statusCode;
		}

		public void setStatusCode(String statusCode) {
			this.statusCode = statusCode;
		}

		public String getStatusName() {
			return statusName;
		}

		public void setStatusName(String statusName) {
			this.statusName = statusName;
		}

		public int getOrderType() {
			return orderType;
		}

		
}