
package com.college.entity;

import java.util.Date;



public class JobLine{
	    private String jobLineId;
		private String jobOrderId;
		private String userId;
		private Attachment attachment;
		private String description;
		private double score;
		private String comment;
		private int submitStatus;
		private Date updateDate;
		
		
		
		public JobLine(String jobLineId, String jobOrderId, String userId,
				Attachment attachment, String description, double score,
				String comment, int submitStatus, Date updateDate) {
			super();
			this.jobLineId = jobLineId;
			this.jobOrderId = jobOrderId;
			this.userId = userId;
			this.attachment = attachment;
			this.description = description;
			this.score = score;
			this.comment = comment;
			this.submitStatus = submitStatus;
			this.updateDate = updateDate;
		}
		
		
		public JobLine() {
		}




		public JobLine(String jobLineId, double score, String comment) {
			super();
			this.jobLineId = jobLineId;
			this.score = score;
			this.comment = comment;
		}


		public String getJobLineId() {
			return jobLineId;
		}
		public void setJobLineId(String jobLineId) {
			this.jobLineId = jobLineId;
		}
		public String getJobOrderId() {
			return jobOrderId;
		}
		public void setJobOrderId(String jobOrderId) {
			this.jobOrderId = jobOrderId;
		}
		public String getUserId() {
			return userId;
		}
		public void setUserId(String userId) {
			this.userId = userId;
		}
		public Attachment getAttachment() {
			return attachment;
		}
		public void setAttachment(Attachment attachment) {
			this.attachment = attachment;
		}
		public String getDescription() {
			return description;
		}
		public void setDescription(String description) {
			this.description = description;
		}
		public double getScore() {
			return score;
		}
		public void setScore(double score) {
			this.score = score;
		}
		public String getComment() {
			return comment;
		}
		public void setComment(String comment) {
			this.comment = comment;
		}
		public int getSubmitStatus() {
			return submitStatus;
		}
		public void setSubmitStatus(int submitStatus) {
			this.submitStatus = submitStatus;
		}
		public Date getUpdateDate() {
			return updateDate;
		}
		public void setUpdateDate(Date updateDate) {
			this.updateDate = updateDate;
		}
		
		
		
		
	
		
}