package com.college.entity;

/**
 * 院校实体类
 * @author bob
 *
 */

public class College {

	
	private String college_id;
	private String college;
	private String university_id;
	public String getCollege_id() {
		return college_id;
	}
	public void setCollege_id(String college_id) {
		this.college_id = college_id;
	}
	public String getCollege() {
		return college;
	}
	public void setCollege(String college) {
		this.college = college;
	}
	public String getUniversity_id() {
		return university_id;
	}
	public void setUniversity_id(String university_id) {
		this.university_id = university_id;
	}
	
}
