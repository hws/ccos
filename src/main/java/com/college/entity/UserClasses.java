package com.college.entity;


/**
 * 用户实体
 * 
 * @author bob
 * 
 */

public class UserClasses {

	/**
	 * 用户ID
	 */
	private String userId;
	
	/**
	 * 班级id
	 */
	private String classId;
	
	private String className;
	
	private String courseName;
	
	private String createdTime;
	
	private String classCode;
	
	private String courseId;//课程id

	
	

	public UserClasses() {
		super();
	}



	

	public UserClasses(String userId, String classId, String className,
			String courseName, String classCode) {
		super();
		this.userId = userId;
		this.classId = classId;
		this.className = className;
		this.courseName = courseName;
		this.classCode = classCode;
	}
	


	public UserClasses(String userId, String classId, String courseName,String courseId) {
		super();
		this.userId = userId;
		this.classId = classId;
		this.courseName = courseName;
		this.courseId = courseId;
	}



	public String getClassCode() {
		return classCode;
	}



	public void setClassCode(String classCode) {
		this.classCode = classCode;
	}



	public String getClassName() {
		return className;
	}



	public void setClassName(String className) {
		this.className = className;
	}



	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getClassId() {
		return classId;
	}

	public void setClassId(String classId) {
		this.classId = classId;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public String getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(String createdTime) {
		this.createdTime = createdTime;
	}
	
	


}
