
package com.college.exception;

/**
 * @author bob
 * 
 */
public class UploadException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UploadException(String msg) {
		super(msg);
	}
}
