
	
	 <!-- js placed at the end of the document so the pages load faster -->
    <script src="${TEMPLATE_PATH}/commStaticResources/baseJs/jquery-1.10.2.min.js"></script>
    <script src="${TEMPLATE_PATH}/commStaticResources/baseFrameWorkUI/bootstrap-3.0.3-dist/dist/js/bootstrap.min.js"></script>
    <script src="${TEMPLATE_PATH}/commStaticResources/baseFrameWorkUI/bootstrapValidator/js/bootstrapValidator.min.js"></script>
    <script src="${TEMPLATE_PATH}/commStaticResources/baseJs/datePicker/bootstrap-datetimepicker.min.js"></script>
    <script src="${TEMPLATE_PATH}/commStaticResources/baseJs/datePicker/bootstrap-datetimepicker.zh-CN.js"></script>
    <!--common script for all pages-->
    <script src="${TEMPLATE_PATH}/commStaticResources/baseJs/jquery.form.min.js"></script> 
    
    
    <!--bootstrap pagination plugin
    <script src="${TEMPLATE_PATH}/commStaticResources/baseJs/bootstrap-paginator.min.js"></script> -->
    <script src="${TEMPLATE_PATH}/commStaticResources/baseJs/jquery.bootpag.min.js"></script> 
    <!--left menu-->
    <script src="${TEMPLATE_PATH}/commStaticResources/baseJs/jquery.dcjqaccordion.2.7.js"></script> 
    <script src="${TEMPLATE_PATH}/commStaticResources/baseJs/bootbox.min.js"></script> 
    <script src="${TEMPLATE_PATH}/teacher/js/left-barMenu.js"></script> 
    
    <script src="${TEMPLATE_PATH}/commStaticResources/baseJs/ui.tab.js"></script> 
    <script src="${TEMPLATE_PATH}/commStaticResources/baseJs/shaixuan.js"></script> 
</body>
</html>

