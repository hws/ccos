<#assign menu="myHomeworkIndex">
<#assign submenu="">
<#include "/commStaticResources/basePage/header.ftl">

<#include "teacherMenue.ftl">
 
    
    
    <div style="margin-left:150px;">
			
             
             
             
			<div class="col-lg-11">
				<section class="panel">
					<header class="panel-heading"> 班级：${summaryJobOrder.className}  </header>
					<header class="panel-heading"> 课程：${summaryJobOrder.courseName}  </header>
					<header class="panel-heading"> 作业：${summaryJobOrder.homeWorkName} </header>
					<header class="panel-heading"> 已提交人数：${jobLineSummaryInfo.submittedCount} </header>
					<header class="panel-heading"> 未提交人数：${jobLineSummaryInfo.totalNum - jobLineSummaryInfo.submittedCount} </header>
					<div class="panel-body">
						<div class="adv-table">
							<div role="grid" class="dataTables_wrapper" id="hidden-table-info_wrapper">
								<table class="table table-striped table-advance table-hover">
                              <thead>
                                  <tr>
		                            <th>姓名</th>
		                            <th>学号</th>
		                            <th>提交时间</th>
		                            <th>作业附件
			                            <a href="javascript:void(0);" class="#" value="${basePath}/user/teacher/download.json?jobOrderId=${summaryJobOrder.jobOrderId}" title="批量下载">
              								<form action="${basePath}/user/teacher/download.json" method="GET">
              									<input name = "jobOrderId" value = "${summaryJobOrder.jobOrderId}" type="hidden">
              									<button class="btn btn-danger btn-xs" type="submit" >
              										<i class="icon-cloud-download "></i>
              									</button>
              								</form>
			                  			</a>
		                  			</th>
		                            <th>评分</th>
		                            <th>评语</th>
		                            <th>操作</th>
                          		</tr>
                                </thead>
                              <tbody role="alert" aria-live="polite" aria-relevant="all">
                              	<#if (stuHomeWorkDetailList?exists && stuHomeWorkDetailList?size>0) >
	                                 <#list stuHomeWorkDetailList as list>
		                            		<tr class="gradeA odd">
		                                    	<input type = "hidden" value="${list.jobLineId}" id="jobLindIdVal">
		                                    	<!--<input type="hidden" value = ${list.attachmentVo.attachmentId} id="attachmentVal" />-->
		                                    	<td>${list.realName}</td>
		                                    	<td>409190217</td>
		                                    	<td>${list.updateDate?string("yyyy-MM-dd HH:mm:ss")}</td>
		                                    	<td>${list.attachmentVo.name}</td>
		                                    	<td><#if list.score != 0 >${list.score}<#else>没有评分</#if></td>
		                                    	<td><#if list.comment??>${list.comment}<#else>没有评语</#if></td>
		                                    	<td>
		                  							<!-- Icons -->
		                							<a href="javascript:void(0);" class="js_homeWork_update_status" jobLineId="${list.jobLineId}" title="把作业【】放进回收站?">
		                  								<button class="btn btn-danger btn-xs">
		                  									<i class="icon-trash "></i>
		                  								</button>
		                  							</a>
		                  							
		                  							<a href="javascript:void(0);" class="jobLine_update_comment" value="${basePath}/user/teacher/jobLine/comment.htm?jobLineId=123" title="评分">
		                  								<button class="btn btn-danger btn-xs">
		                  									<i class="icon-pencil "></i>
		                  								</button>
		                  							</a>
		                  							
		                						</td>
		                                	</tr>
	                            	  </#list>
                            	  </#if>
                              	
                              	  <#if (!stuHomeWorkDetailList?exists || stuHomeWorkDetailList?size <= 0) >
                              	  	<tr class="gradeA odd">
                              	  		<td>暂无学生提交的作业信息</td>
                              	  	</tr>
                                	
                                   </#if>
                                   
                                </tbody>
                              </table>
                              <div style="height: 30px;">
                              <div class="pagination"><ul class="pagination"></ul></div>
                              </div>
                           </div>
						</div>
					</div>
					
					<div class="pagination"><ul class="pagination"><li class="active"><a href="?p=1" class="number current" title="1">1</a></li><li><a href="?p=2" class="number" title="2">2</a></li><li><a href="?p=2" title="下一页">&gt;</a></li><li><a href="?p=2" title="末页">&gt;&gt;</a></li></ul></div>
			
				</section>
				
				
			</div>
			<!-- page end-->
			
			
			
			
	</div>
	
	

	
	<#include "teacherFooter.ftl">
	
	<script>
$(function(){
	
	$('.jobLine_update_comment').click(function(){
		/*bootbox.dialog({
			message : "更新学生作业评分",
			title : "提示",
			buttons : {
				"delete" : {
					label : "确定",
					className : "btn-success",
					callback : function() {
						$.post("${basePath}/admin/article/status/delete.json", {},function(data){
									bootbox.alert("清理成功,即将刷新页面", function() {
										window.location.reload();
									});
						}, "json");
					}
				},
				"cancel" : {
					label : "取消",
					className : "btn-primary",
					callback : function() {
					
					}
				}
			}
		});*/
		
		
		
		bootbox.dialog({
		    title : "修改评分",
		    message : "<div class='well ' style='margin-top:25px;'><form class='form-horizontal' role='form'><div class='form-group'><label class='col-sm-3 control-label no-padding-right' for='txtOldPwd'>分数</label><div class='col-sm-9'><input type='text' id='txtOldPwd' placeholder='请输入分数' class='col-xs-10 col-sm-5' /></div></div><div class='space-4'></div><div class='form-group'><label class='col-sm-3 control-label no-padding-right' for='txtNewPwd1'>评语</label><div class='col-sm-9'><textarea type='text' id='txtNewPwd1' placeholder='请输入评语' class='col-xs-10 col-sm-5' /></div></div><div class='space-4'></div><div class='form-group'><div class='col-sm-9'></div></div></form></div>",
		    buttons : {
		        "success" : {
		            "label" : "<i class='icon-ok'></i> 保存",
		            "className" : "btn-sm btn-success",
		            "callback" : function() {
		                var score = $("#txtOldPwd").val().trim();
		                var comment = $("#txtNewPwd1").val().trim();
		                
		 
		                if(score.length <= 0  && comment.length <= 0 ){
		                    bootbox.alert("评分项不能为空");
		                    return false;
		                }else if(score.length <= 0 ){
		                	bootbox.alert("分数不能为空");
		                    return false;
		                }else if(comment.length <= 0){
		                	bootbox.alert("评语不能为空");
		                    return false;
		                }
		                var info = {"jobLineId":$("#jobLindIdVal").val(),"score":score,"comment":comment};
		                $.post("${basePath}/user/teacher/updateStuJobLine.json",info,function(data){
		                	if(data.result){
								 bootbox.alert("修改成功");
		                   		 window.location.reload();
							}else{
								 bootbox.alert("修改失败，请重试");
							}
		                   
		                },'json');
		            }
		        },
		        "cancel" : {
		            "label" : "<i class='icon-info'></i> 取消",
		            "className" : "btn-sm btn-danger",
		            "callback" : function() { }
		        }
		    }
		})
	});
	
	$('.js_homeWork_update_status').click(function(){
		var jobOrderId = $(this).attr('jobOrderId');
		var orderStatus= "trash";
		bootbox.dialog({
			message : "是否"+$(this).attr('title'),
			title : "提示",
			buttons : {
				"delete" : {
					label : "确定",
					className : "btn-success",
					callback : function() {
						$.post("${basePath}/user/teacher/myReleasedHomeWorkStatus/update.json", { "jobOrderId": jobOrderId,"orderStatus": orderStatus},function(data){
							if(data.result){
								window.location.reload();
							}	
								
						}, "json");
					}
				},
				"cancel" : {
					label : "取消",
					className : "btn-primary",
					callback : function() {
					
					}
				}
			}
		});					
	});			
});
</script>
    

