<aside>
			<div id="sidebar" class="nav-collapse " tabindex="5000" style="overflow: hidden; outline: none;">
				<!-- sidebar menu goes here-->
				<ul class="sidebar-menu" id="nav-accordion">
					<li class="">
						<a href="${TEMPLATE_PATH}/user/teacher/index.htm" <#if menu="myHomeworkIndex">class="active"</#if>> <i class="icon-home"></i> <span>我的作业本</span></a>
					</li>
					<li class="sub-menu">
						<a href="${TEMPLATE_PATH}/user/teacher/releaseHomeWork.htm" <#if menu="releaseIndex">class="active"</#if> > <i class="icon-folder-open"></i> <span>发布作业</span></a>						
					</li>
					
					<li class="sub-menu">
						<a href="javascript:;" <#if menu="setting">class="active"</#if> > <i class="icon-cogs"></i> <span>设置</span><span class="dcjq-icon"></span></a>
						<ul class="sub">
							<li  <#if submenu="classCourseManage">class="active"</#if> ><a href="${TEMPLATE_PATH}/user/teacher/classCourseManage.htm" >班级课程管理</a></li>
							<li><a href="${TEMPLATE_PATH}/user/teacher/profile.htm">修改个人信息</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</aside>