<#assign menu="releaseIndex">
<#assign submenu="">
<#include "/commStaticResources/basePage/header.ftl">

<#include "teacherMenue.ftl">
 
    
<section id="main-content">
	
		
		<div class="row">
			<div class="col-lg-8">
				<section class="panel">
					<header class="panel-heading"> 发布作业信息 </header>
					<div class="panel-body">
		
						<form method="post" id="releaseHomeWorkForm" action="${TEMPLATE_PATH}/user/teacher/releaseHomeWork.json" class="form-horizontal" enctype="multipart/form-data">
				<fieldset>
					<div class="form-group">
						<label class="col-lg-3 control-label">班级名称</label>
						<div class="col-lg-6">
							<select class="form-control" name="classId"  id="classNameSelector" >
								<option value="">-- 请选择班级 --</option>
								
							</select>
						</div>
					</div>
					
				<div class="form-group">
							<label class="col-lg-3 control-label">课程名称</label>
							<div class="col-lg-6">
								<select class="form-control" name="courseId"  id="courseSelector" >
									<option value="">-- 请选择课程 --</option>
									
								</select>
							</div>
						</div>
		
						<div class="form-group">
							<label class="col-lg-3 control-label" for="email">作业名称</label>
								<div class="col-lg-6">
								<input type="text" class="form-control" id="homeWorkName" name="homeWorkName"   >
							</div>
						</div>
		
						<div class="form-group">
								<label class="col-lg-3 control-label" for="password">作业描述</label>
									<div class="col-lg-6">
									<textarea class="form-control" id="hwDescriptionId" name="homeWorkDescripton" rows="3"></textarea>
								</div>
						</div>
						
						<div class="form-group">
							<label class="col-lg-3 control-label" >开始日期</label>
							<div class="col-lg-6">
                                <div class="input-group date" id="datetimePicker">
                                    <input type="text" class="form-control" name="startDate" id="datetimepicker_startDate" data-date-format="yyyy-mm-dd hh:ii">
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                </div>
                            </div>
						</div>
					
						<div class="form-group">
							<label class="col-lg-3 control-label" >截止日期</label>
							<div class="col-lg-6">
                                <div class="input-group date" id="datetimePicker">
                                    <input type="text" class="form-control" name="endDate" id="datetimepicker_endDate" data-date-format="yyyy-mm-dd hh:ii" >
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                </div>
                            </div>
						</div>
						
						<!--上传附件-->
							<div class="form-group">
								<label class="col-lg-3 control-label" >上传附件</label>
								<div class="col-lg-6">
									<input class="short" id="user_last_name" name="file"
										size="30" type="file" value="">
								</div>
							</div>
						<!--end-上传附件-->
						
						<div class="col-lg-9 col-lg-offset-3">
							<button type="submit" class="btn btn-primary">保存发布</button>
					    </div>

				</fieldset>

			</form>
					</div>
				</section>
				</div>
				
					
			
				
			</div>
			
			<!-- page end-->
		
			
		</div>
</section>
 

 <#include "teacherFooter.ftl">
 <script type="text/javascript">
    $('#datetimepicker_startDate').datetimepicker({
        language:  'zh-CN',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		forceParse: 0,
        showMeridian: 1
    });
    $('#datetimepicker_endDate').datetimepicker({
        language:  'zh-CN',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		forceParse: 0,
        showMeridian: 1
    });
    
</script>
<!--<script src="${TEMPLATE_PATH}/stuAdmin/js/createClassPage.js"></script> -->
<script src="${TEMPLATE_PATH}/teacher/js/releaseFormValidation.js"></script>
<script src="${TEMPLATE_PATH}/teacher/js/teacherRelease.js"></script>

