<#assign menu="myHomeworkIndex">
<#assign submenu="">
<#include "/commStaticResources/basePage/header.ftl">

<#include "teacherMenue.ftl">
 
    
    
    <div style="margin-left:150px;">
			<div class="row">
                  <div class="col-lg-12">
                      <!--breadcrumbs start 
                      <ul class="breadcrumb">
                      <li><a href="http://localhost:8081/cms/admin/article/page.htm?status=display">作业状态：  </li>
                          <li><a href="http://localhost:8081/cms/admin/article/page.htm?status=display">已发布</a>（4）</li>
						<li><a href="http://localhost:8081/cms/admin/article/page.htm?status=trash">回收站</a>（2）</li>
                      </ul>-->
                      
                      <!--多条件搜索-->
                      <div class="w1200">
                      	<form id="searchHomeworkForm" action="${TEMPLATE_PATH}/user/teacher/searchHomeWorkList.json" method="post">
						  <div class="list-screen">
						    <div class="screen-top" style="position:relative;">
						    	<span>作业名称<input id="txtadress" type="text" name="homeWorkName"></span>
						    	<span>开始时间<input type="text" class="mh_date" readonly="true" name="startDate"></span>
						    	<span>截止时间<input type="text" class="mh_date" readonly="true" name="endDate"></span>
						    	<!--<a href="#" type = "submit" id="submit-btn">搜索</a>-->
						    	<button  type="submit" id="submit-btn">搜索</button>
						    </div>
						    <div style="padding:10px 30px 10px 10px;"><div class="screen-address">
						      <div class="list-tab">
						        
						      </div>
						    </div>
						    <div class="screen-term">
						      <div class="selectNumberScreen">
						        <div id="selectList" class="screenBox screenBackground">
						          <dl class="listIndex" >
						            <dt>课程</dt>
						            <dd>
						              <label><a href="javascript:;" values2="" values1="" attrval="不限">不限</a></label>
						              <#if (courseList?exists && courseList?size>0) >
						                  <#list courseList as list>
		                        			 <label>
								               	 <input name="courseId" type="radio" value="${list.courseId}">
								                <a href="javascript:;"  attrval="1-99">${list.courseName}</a>
							                 </label>
		                    	 		 </#list>
						               </#if>
						            </dd>
						          </dl>
						          <dl class=" listIndex" attr="terminal_os_s">
						            <dt>作业状态</dt>
						            <dd>
						              <label><a href="javascript:;" values2="" values1="" attrval="不限">不限</a> </label>
						              <label>
						                <input name="orderStatus" type="radio" value="4" autocomplete="off">
						                <a href="javascript:;" values2="" values1="" >进行中</a> </label>
						              <label>
						                <input name="orderStatus" type="radio" value="8" autocomplete="off">
						                <a href="javascript:;" values2="" values1="" >关闭</a></label>
						              <label>
						                <input name="orderStatus" type="radio" value="11" autocomplete="off">
						                <a href="javascript:;" values2="" values1="" >审批中</a></label>
						              <label>
						                <input name="orderStatus" type="radio" value="12" autocomplete="off">
						                <a href="javascript:;" values2="" values1="" >完成/结束</a></label>
						            </dd>
						          </dl>
						          <dl class="listIndex" attr="terminal_brand_s">
						            <dt>作业类型</dt>
						            <dd data-more="true">
						              <label><a href="javascript:;" values2="" values1="" attrval="不限">不限</a></label>
						              <label>
						                <input name="orderType" type="radio" value="9" autocomplete="off">
						                <a href="javascript:;" values2="" values1="" attrval="小米">线上</a></label>
						              <label>
						                <input name="orderType" type="radio" value="10" autocomplete="off">
						                <a href="javascript:;" values2="" values1="" attrval="华为">线下</a> </label>
						             
						            </dd> 
						          </dl>
						         
						        </div>
						      </div>   
						    </div>
						
						    </div>
						    
						    <div class="hasBeenSelected clearfix"><span id="time-num"><font>${releasedHomeWorkPage.count}</font>&nbsp;个作业</span>
						    	  <input type="hidden" name="totalPageNum" value="${releasedHomeWorkPage.pageCount}" />
						          <div style="float:right;" class="eliminateCriteria">【清空全部】 </div>
						          <dl>
						            <dt>已选条件：</dt>
						            <dd style="" class="clearDd">
						              <div class="clearList"></div>
						          </dd></dl>
						        </div>
						      <!--<script type="text/javascript" src="JS/shaixuan.js"></script> -->
						  </div>
						  </form>
						</div>
                  </div>
                  <!--多条件搜索 end-->
             </div>
             
             
             
			<div class="col-lg-11">
				<section class="panel">
					<header class="panel-heading"> 所有作业列表 </header>
					<div class="panel-body">
						<div class="adv-table">
							<div role="grid" class="dataTables_wrapper" id="hidden-table-info_wrapper">
								<table class="table table-striped table-advance table-hover">
                              <thead>
                                  <tr>
		                            <th>作业名称</th>
		                            <th>班级名称</th>
		                            <th>课程名称</th>
		                            <th>开始时间</th>
		                            <th>截止时间</th>
		                            <th>状态</th>
		                            <th>类型</th>
		                            <th>操作</th>
                          		</tr>
                                </thead>
                              <tbody role="alert" aria-live="polite" aria-relevant="all">
                                 <#list releasedHomeWorkPage.list as list>
	                            		<tr class="gradeA odd">
	                                    	
	                                    	<td>${list.homeWorkName}</td>
	                                    	<td>${list.className}</td>
	                                    	<td>${list.courseName}</td>
	                                    	<td>${list.startDate}</td>
	                                    	<td>${list.endDate}</td>
	                                    	<td>${list.statusName}</td>
	                                    	<td>类型</td>
	                                    	<td>
	                  							<!-- Icons -->
	                  							<input type="hidden" value="${list.jobOrderId}" id="jobOrderId"/>
	                							<a href="javascript:void(0);" class="js_homeWork_update_status" jobOrderId="${list.jobOrderId}" title="把作业【${list.homeWorkName}】放进回收站?">
	                  								<button class="btn btn-danger btn-xs">
	                  									<i class="icon-trash "></i>
	                  								</button>
	                  							</a>
	                  							<a href="${basePath}/user/teacher/classesCourseManage/update.htm?" title="修改">
	                  								<button class="btn btn-danger btn-xs">
	                  									<i class="icon-pencil "></i>
	                  								</button>
	                  							</a>
	                  							
	                  							<a href="${basePath}/user/teacher/queryStuJobLinesDetail.htm?jobOrderId=${list.jobOrderId}&homeWorkName=${list.homeWorkName}&className=${list.className}&courseName=${list.courseName}" title="学生作业明细">
	                  								<button class="btn btn-danger btn-xs">
	                  									<i class=" icon-list "></i>
	                  								</button>
	                  							</a>
	                  							
	                						</td>
	                                	</tr>
                            	  </#list>
                              
                                
                                </tbody>
                              </table>
                              
                              <!--test-->
  							  <div id="page-selection"></div>
                           </div>
						</div>
					</div>
					
				</section>
				
				
			</div>
			<!-- page end-->
			
			
			
			
	</div>
	
	

	
	<#include "teacherFooter.ftl">
	 <script src="${TEMPLATE_PATH}/teacher/js/searchPagination.js"></script> 
	
	<!-- <script  type='text/javascript'>
	var total = ${releasedHomeWorkPage.count};
 function bootAjax(totalPageNum){
	 $('#page-selection').bootpag({
            total:total,
            maxVisible: 6
        }).on("page", function(event, num){
             $("#content").html("Insert content"+num); // some ajax content loading...
			$.post(
					"${basePath}/user/teacher/searchHomeWorkList.json", 
					{ 	
						"homeWorkName":$('input[name=homeWorkName]').val(),
						"startDate":$('input[name=startDate]').val(),
						"endDate":$('input[name=endDate]').val(),
						"courseId":$('input[name=courseId]').val(),
						"orderStatus":$('input[name=orderStatus]').val(),
						"orderType":$('input[name=orderType]').val(),
						"p":num
					},
					function(data){
						$('.adv-table table  tbody tr').empty();
						if(data.pageVo.list.length > 0 ){
						var html = '';
						 for(var i =0 , l=data.pageVo.list.length ; i < l ; i++){
						 	var entity = data.pageVo.list[i];
						 	console.log(entity);
							html += '<tr class="gradeA odd">'+
										'<td>'+entity.homeWorkName+'</td>' +
										'<td>'+entity.className+'</td>' +
										'<td>'+entity.courseName+'</td>' +
										'<td>'+entity.startDate+'</td>' +
										'<td>'+entity.endDate+'</td>' +
										'<td>'+entity.statusName+'</td>' +
										'<td>类型</td>' +
										'<td>'+
		          							'<input type="hidden" value="'+entity.jobOrderId+'" id="jobOrderId"/>' +
		            							'<a href="javascript:void(0);" class="js_homeWork_update_status" jobOrderId="'+entity.jobOrderId+'" title="把作业【'+entity.homeWorkName+'】放进回收站?"> ' +
		              								'<button class="btn btn-danger btn-xs">' +
		              									'<i class="icon-trash "></i>' +
		              								'</button>' +
		              							'</a>&nbsp' +
		              							'<a href="'+ctx+'/user/teacher/classesCourseManage/update.htm?" title="修改">' +
		              								'<button class="btn btn-danger btn-xs">' +
		              									'<i class="icon-pencil "></i>' +
		              								'</button>' +
		              							'</a>&nbsp' +
		              							'<a href="'+ctx+'/user/teacher/queryStuJobLinesDetail.htm?jobOrderId='+entity.jobOrderId+'&homeWorkName='+entity.homeWorkName+'&className='+entity.className+'&courseName='+entity.courseName+'" title="学生作业明细">' +
		              								'<button class="btn btn-danger btn-xs">' +
		              									'<i class=" icon-list "></i>' +
		              								'</button>' +
		              							'</a>&nbsp' +
		            					 '</td>' +
										'</tr>';
								
							console.log(html);
						 }
						$('.adv-table table tbody').append(html);	
						initUpdateHomeWorkStatus();	
			  }	
			}, "json");
        });
   };     
  function initUpdateHomeWorkStatus(){
	  $('.js_homeWork_update_status').click(function(){
		var jobOrderId = $(this).attr('jobOrderId');
		var orderStatus= "trash";
		bootbox.dialog({
			message : "是否"+$(this).attr('title'),
			title : "提示",
			buttons : {
				"delete" : {
					label : "确定",
					className : "btn-success",
					callback : function() {
						$.post("${basePath}/user/teacher/myReleasedHomeWorkStatus/update.json", { "jobOrderId": jobOrderId,"orderStatus": orderStatus},function(data){
								window.location.reload();
						}, "json");
					}
				},
				"cancel" : {
					label : "取消",
					className : "btn-primary",
					callback : function() {
					
					}
				}
				}
			});					
		});		
};




$(function(){
	var total = ${releasedHomeWorkPage.count};
	bootAjax(total);

	initUpdateHomeWorkStatus();
	//根据搜索条件查找作业
	$('#searchHomeworkForm').ajaxForm({
		dataType : 'json',
		success : function(data) {
			console.log(data);
			console.log(data.pageVo);
			console.log("pageNumHtml:"+data.pageVo.pageNumHtml);
			$('.adv-table table  tbody tr').empty();
			$('#time-num font').html(data.pageVo.count);
			bootAjax(data.pageVo.count);
			if(data.pageVo.list.length > 0 ){
				var html = '';
				 for(var i =0 , l=data.pageVo.list.length ; i < l ; i++){
				 	var entity = data.pageVo.list[i];
				 	console.log(entity);
					html += '<tr class="gradeA odd">'+
								'<td>'+entity.homeWorkName+'</td>' +
								'<td>'+entity.className+'</td>' +
								'<td>'+entity.courseName+'</td>' +
								'<td>'+entity.startDate+'</td>' +
								'<td>'+entity.endDate+'</td>' +
								'<td>'+entity.statusName+'</td>' +
								'<td>类型</td>' +
								'<td>'+
          							'<input type="hidden" value="'+entity.jobOrderId+'" id="jobOrderId"/>' +
            							'<a href="javascript:void(0);" class="js_homeWork_update_status" jobOrderId="'+entity.jobOrderId+'" title="把作业【'+entity.homeWorkName+'】放进回收站?"> ' +
              								'<button class="btn btn-danger btn-xs">' +
              									'<i class="icon-trash "></i>' +
              								'</button>' +
              							'</a>&nbsp' +
              							'<a href="'+ctx+'/user/teacher/classesCourseManage/update.htm?" title="修改">' +
              								'<button class="btn btn-danger btn-xs">' +
              									'<i class="icon-pencil "></i>' +
              								'</button>' +
              							'</a>&nbsp' +
              							'<a href="'+ctx+'/user/teacher/queryStuJobLinesDetail.htm?jobOrderId='+entity.jobOrderId+'&homeWorkName='+entity.homeWorkName+'&className='+entity.className+'&courseName='+entity.courseName+'" title="学生作业明细">' +
              								'<button class="btn btn-danger btn-xs">' +
              									'<i class=" icon-list "></i>' +
              								'</button>' +
              							'</a>&nbsp' +
            					 '</td>' +
								'</tr>';
						
					console.log(html);
				 }
				$('.adv-table table tbody').append(html);	
				initUpdateHomeWorkStatus();		
			}
		}
	});
	
});
</script>-->
    

