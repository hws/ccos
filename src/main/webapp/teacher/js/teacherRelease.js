/**
 * 
 */

$().ready(function() {
	releaseHomeWork();
	/* get the class name */
	$.ajax({
				type : "get",
				dataType : "json",
				url : ctx + "/user/system/queryMyClasses.json",
				data : {
						// id:123//老师id查询所教班级，后台session获取
				},
				complete : function() {
				},
				success : function(data) {
					// alert(data);
					var jsons = new Array();
					var classNameHtml = "";
					if (data != undefined) {
						$.each(data, function(index, item) {
							classNameHtml += "<option value='" + item.classId +"'>"
									+ item.className + "</option>";
								// jsons.push(data[0].class_name);
							});
						$(classNameHtml).appendTo("#classNameSelector");
					};
				},
				error : function() {
					// messageDialog($.i18n.prop("error.message"));
				}
			});

	// 根据班级级联查询课程
	$("#classNameSelector").change(function() {
				var name = $("#classNameSelector").val().split('_');
				var classId = name[0];
				if (classId != undefined) {
					/* ZTree */
					// ajaxForSection('show','','',ipId,'');//call the ajax
					// function
					queryCourseByClassId(classId);
				}

			});

});

function queryCourseByClassId(parameter) {
	$("#courseSelector").find('option:not(:first)').remove();
	$.ajax({
				type : "get",
				dataType : "json",
				url : ctx + "/user/system/getCourseListByClassId.json",
				data : {
					classId : parameter
					// id:123//老师id查询所教班级，后台session获取
				},
				complete : function() {
				},
				success : function(data) {
					var jsons = new Array();
					var courseNameHtml = "";
					if (data != undefined) {
						$.each(data, function(index, item) {
							courseNameHtml += "<option value='"
									+ item.courseId + "'>" + item.courseName + "</option>";
							});
						$(courseNameHtml).appendTo("#courseSelector");
					};
				},
				error : function() {
					//messageDialog($.i18n.prop("error.message"));
				}
			});
}
function releaseHomeWork(){
	
	$('#releaseHomeWorkForm').ajaxForm({
		dataType : 'json',
		success : function(data) {
			if (data.result) {
				bootbox.alert("发布成功，将刷新页面", function() {
					window.location.reload();
				});
			}else{
				showErrors($('#releaseHomeWorkForm'),data.errors);
			}
		}
	});
}

