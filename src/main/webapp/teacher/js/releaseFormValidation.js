$(document).ready(function() {
    $('#releaseHomeWorkForm').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
        	classesName : {
				validators : {
					notEmpty : {
						// message: 'The country is required and can\'t be
						// empty'
						message : '请选择班级'
					}
				}
			},
           
			homeWorkName : {
				message : 'The username is not valid',
				validators : {
					notEmpty : {
						// message: 'The username is required and can\'t be
						// empty'
						message : '作业名称不能为空'
					},
					stringLength : {
						min : 2,
						max : 50,
						// message: 'The username must be more than 6 and less
						// than 30 characters long'
						message : '作业名称必须大于 2 并且少于  50 个字符长度'
					}
				/*
				 * , regexp: { regexp: /^[a-zA-Z0-9_\.]+$/, message:
				 * 'The username can only consist of alphabetical,
				 * number, dot and underscore' }
				 */
				}
			},
			
			courseName : {
				validators : {
					notEmpty : {
						// message: 'The country is required and can\'t be
						// empty'
						message : '请选择课程'
					}
				}
			},
           
			homeWorkDescripton: {
	                message: 'The username is not valid',
	                validators: {
	                    notEmpty: {
	                        message: '作业描述说明不能为空'
	                    },
	                    stringLength: {
	                        min: 1,
	                        max: 300,
	                        message: '作业描述说明长度必须大于0'
	                    }
	                }
	        },
	        
	        startDate:{
                message: 'The username is not valid',
                validators: {
                    notEmpty: {
                        message: '作业布置日期不能为空'
                    }
                }
            },
            endDate:{
                message: 'The username is not valid',
                validators: {
                    notEmpty: {
                        message: '作业截止日期不能为空'
                    }
                }
            }
			
        }
    });
});



