function bootAjax(totalPageNum){
	 $('#page-selection').bootpag({
            total:totalPageNum,
            maxVisible: 6,
            page: 1
        }).on("page", function(event, num){
             $("#content").html("Insert content"+num); // some ajax content loading...
			$.post(
					ctx+'/user/teacher/searchHomeWorkList.json', 
					{ 	
						"homeWorkName":$('input[name=homeWorkName]').val(),
						"startDate":$('input[name=startDate]').val(),
						"endDate":$('input[name=endDate]').val(),
						"courseId":$('input[name=courseId]:checked').val(),
						"orderStatus":$('input[name=orderStatus]:checked').val(),
						"orderType":$('input[name=orderType]:checked').val(),
						"p":num
					},
					function(data){
						$('.adv-table table  tbody tr').empty();
						if(data.pageVo.list.length > 0 ){
						var html = '';
						 for(var i =0 , l=data.pageVo.list.length ; i < l ; i++){
						 	var entity = data.pageVo.list[i];
						 	console.log(entity);
							html += '<tr class="gradeA odd">'+
										'<td>'+entity.homeWorkName+'</td>' +
										'<td>'+entity.className+'</td>' +
										'<td>'+entity.courseName+'</td>' +
										'<td>'+entity.startDate+'</td>' +
										'<td>'+entity.endDate+'</td>' +
										'<td>'+entity.statusName+'</td>' +
										'<td>类型</td>' +
										'<td>'+
		          							'<input type="hidden" value="'+entity.jobOrderId+'" id="jobOrderId"/>' +
		            							'<a href="javascript:void(0);" class="js_homeWork_update_status" jobOrderId="'+entity.jobOrderId+'" title="把作业【'+entity.homeWorkName+'】放进回收站?"> ' +
		              								'<button class="btn btn-danger btn-xs">' +
		              									'<i class="icon-trash "></i>' +
		              								'</button>' +
		              							'</a>&nbsp' +
		              							'<a href="'+ctx+'/user/teacher/classesCourseManage/update.htm?" title="修改">' +
		              								'<button class="btn btn-danger btn-xs">' +
		              									'<i class="icon-pencil "></i>' +
		              								'</button>' +
		              							'</a>&nbsp' +
		              							'<a href="'+ctx+'/user/teacher/queryStuJobLinesDetail.htm?jobOrderId='+entity.jobOrderId+'&homeWorkName='+entity.homeWorkName+'&className='+entity.className+'&courseName='+entity.courseName+'" title="学生作业明细">' +
		              								'<button class="btn btn-danger btn-xs">' +
		              									'<i class=" icon-list "></i>' +
		              								'</button>' +
		              							'</a>&nbsp' +
		            					 '</td>' +
										'</tr>';
								
							console.log(html);
						 }
						$('.adv-table table tbody').append(html);	
						initUpdateHomeWorkStatus();	
			  }	
			}, "json");
        });
   };     
   
 function initUpdateHomeWorkStatus(){
	  $('.js_homeWork_update_status').click(function(){
		var jobOrderId = $(this).attr('jobOrderId');
		var orderStatus= "trash";
		bootbox.dialog({
			message : "是否"+$(this).attr('title'),
			title : "提示",
			buttons : {
				"delete" : {
					label : "确定",
					className : "btn-success",
					callback : function() {
						$.post(ctx+'/user/teacher/myReleasedHomeWorkStatus/update.json', { "jobOrderId": jobOrderId,"orderStatus": orderStatus},function(data){
								window.location.reload();
						}, "json");
					}
				},
				"cancel" : {
					label : "取消",
					className : "btn-primary",
					callback : function() {
					
					}
				}
				}
			});					
		});		
};

$().ready(function() {
	var total = $('input[name=totalPageNum]').val();
	bootAjax(total);

	initUpdateHomeWorkStatus();
	//根据搜索条件查找作业
	$('#searchHomeworkForm').ajaxForm({
		dataType : 'json',
		success : function(data) {
			$('.adv-table table  tbody tr').empty();
			$('#time-num font').html(data.pageVo.count);
			if(data.pageVo.pageCount > 0 ){
				bootAjax(data.pageVo.pageCount);
			}else{
				$('#page-selection').empty();//remove it
			}
				
			
			if(data.pageVo.list.length > 0 ){
				var html = '';
				 for(var i =0 , l=data.pageVo.list.length ; i < l ; i++){
				 	var entity = data.pageVo.list[i];
				 	console.log(entity);
					html += '<tr class="gradeA odd">'+
								'<td>'+entity.homeWorkName+'</td>' +
								'<td>'+entity.className+'</td>' +
								'<td>'+entity.courseName+'</td>' +
								'<td>'+entity.startDate+'</td>' +
								'<td>'+entity.endDate+'</td>' +
								'<td>'+entity.statusName+'</td>' +
								'<td>类型</td>' +
								'<td>'+
          							'<input type="hidden" value="'+entity.jobOrderId+'" id="jobOrderId"/>' +
            							'<a href="javascript:void(0);" class="js_homeWork_update_status" jobOrderId="'+entity.jobOrderId+'" title="把作业【'+entity.homeWorkName+'】放进回收站?"> ' +
              								'<button class="btn btn-danger btn-xs">' +
              									'<i class="icon-trash "></i>' +
              								'</button>' +
              							'</a>&nbsp' +
              							'<a href="'+ctx+'/user/teacher/classesCourseManage/update.htm?" title="修改">' +
              								'<button class="btn btn-danger btn-xs">' +
              									'<i class="icon-pencil "></i>' +
              								'</button>' +
              							'</a>&nbsp' +
              							'<a href="'+ctx+'/user/teacher/queryStuJobLinesDetail.htm?jobOrderId='+entity.jobOrderId+'&homeWorkName='+entity.homeWorkName+'&className='+entity.className+'&courseName='+entity.courseName+'" title="学生作业明细">' +
              								'<button class="btn btn-danger btn-xs">' +
              									'<i class=" icon-list "></i>' +
              								'</button>' +
              							'</a>&nbsp' +
            					 '</td>' +
								'</tr>';
						
				 }
				$('.adv-table table tbody').append(html);	
				initUpdateHomeWorkStatus();		
			}
		}
	});
});


