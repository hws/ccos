<#assign menu="studentHomeworkIndex">
<#assign submenu="">
<#include "/commStaticResources/basePage/header.ftl">

<#include "studentMenue.ftl">
 
    
    <div style="margin-left:150px;">
			
			<div class="col-lg-12">
				<section class="panel">
					<header class="panel-heading"> 所有作业列表 </header>
					<div class="panel-body">
						<div class="adv-table">
							<div role="grid" class="dataTables_wrapper" id="hidden-table-info_wrapper">
								<table class="table table-striped table-advance table-hover">
                              <thead>
                                  <tr>
		                            <th>作业名称</th>
		                            <th>班级名称</th>
		                            <th>课程名称</th>
		                            <th>开始时间</th>
		                            <th>截止时间</th>
		                            <th>状态</th>
		                            <th>类型</th>
		                            <th>附件</th>
		                            <th>提交状态</th>
		                            <th>操作</th>
                          		</tr>
                                </thead>
                              <tbody role="alert" aria-live="polite" aria-relevant="all">
                                 <#list releasedHomeWorkList as list>
	                            		<tr class="gradeA odd">
	                                    	
	                                    	<td>${list.homeWorkName}</td>
	                                    	<td>${list.className}</td>
	                                    	<td>${list.courseName}</td>
	                                    	<td>${list.startDate}</td>
	                                    	<td>${list.endDate}</td>
	                                    	<td>${list.statusName}</td>
	                                    	<td>类型</td>
	                                    	<td>
	                                    		<a href="${basePath}/user/student/download.json?fileName=${list.attachment.name}&filePath=${list.attachment.path}" target="_blank">${list.attachment.name}</a>
	                                    	</td>
	                                    	<td>
	                                    		<#if list.submitStatus == "2">  
												 	需提交
												</#if>
												<#if list.submitStatus == "1">  
												 	已提交
												</#if>
	                                    	</td>
	                                    	<td>
	                  							
	                  							<input type="hidden" value="${list.jobOrderId}" id="jobOrderId"/>
	                							<!-- Icons 
	                  							<a href="${basePath}/user/teacher/classesCourseManage/update.htm?" title="修改">
	                  								<button class="btn btn-danger btn-xs">
	                  									<i class="icon-pencil "></i>
	                  								</button>-->
	                  							</a>
	                  							
	                  							
	                  							<a  href="javascript:void(0);" class="js_homeWork_update_status" jobLineId="${list.jobLineId}" title="提交作业【${list.homeWorkName}】?">
	                  								<button class="btn btn-danger btn-xs">
	                  									<i class="icon-cloud-upload "></i>
	                  								</button>
	                  							</a>
	                  							
	                						</td>
	                                	</tr>
                            	  </#list>
                              
                                
                                </tbody>
                              </table>
                              <div style="height: 30px;">
                              <div class="pagination"><ul class="pagination"></ul></div>
                              </div>
                           </div>
						</div>
					</div>
					
					<div class="pagination"><ul class="pagination"><li class="active"><a href="?p=1" class="number current" title="1">1</a></li><li><a href="?p=2" class="number" title="2">2</a></li><li><a href="?p=2" title="下一页">&gt;</a></li><li><a href="?p=2" title="末页">&gt;&gt;</a></li></ul></div>
			
				</section>
				
				
			</div>
			<!-- page end-->
			
			
			
			
	</div>
	
	<#include "studentFooter.ftl">
	
<script>
$(function(){

		var globalJobLineId = $(this).attr('jobLineId');
	
	$('.js_homeWork_update_status').click(function(){
		var jobLineId = $(this).attr('jobLineId');
		var submitStatus= "submitted";
	
		bootbox.dialog({
			message : "是否"+$(this).attr('title')+"<br><br><div id='attachment'></div><button id='file_upload'  class='btn btn-shadow btn-info' type='button'><i class='icon-cloud-upload'></i> 添加附件</button>",
			title : "提示",
			buttons : {
				"delete" : {
					label : "确定",
					className : "btn-success",
					callback : function() {
						$.post("${basePath}/user/student/myReleasedHomeWorkStatus/update.json", { "jobLineId": jobLineId,"submitStatus": submitStatus},function(data){
								window.location.reload();
						}, "json");
					}
				},
				"cancel" : {
					label : "取消",
					className : "btn-primary",
					callback : function() {
					
					}
				}
			}
		});			
		
		getAttachment(jobLineId);
			
		$('#file_upload').uploadify({
			'buttonText'  		: 	'上传作业文件',
	        'swf'         		: 	'${basePath}/commStaticResources/baseJs/uploadify/uploadify.swf',
	        'uploader'    		: 	'${basePath}/user/student/jobLineAttachment/upload.json',
	        'formData'  		: 	{'jobLineId':jobLineId},
	        'fileObjName'		: 	'file',
	        'fileTypeExts' 		: 	'*.*',
	        'method'			:	'post',
	        'onUploadSuccess' 	: 	function(file, data, response) {
	        	getAttachment(jobLineId);
	        }
		});		
		
	});			
	
	function getAttachment(jobLineId){
		$.getJSON("${basePath}/user/student/jobLineAttachment/query.json?jobLineId="+jobLineId+"&v="+Math.random(),function(data){
			$('#attachment').html("");
			console.log(data);
			addAttachment(data.attachmentList);
		});
	}
	
	function addAttachment(list){
		var html = '<table class="table"><thead><tr><th>文件名</th><th>大小</th><th>操作</th></tr></thead><tbody>';
		for(i=0;i<list.length;i++){
			var attachment = list[i];
			html += '<tr>';
			if(attachment.type=="photo"){
				html += '<td><img src="${basePath}/'+attachment.path+'" width="200"/></td>';
			}else{
				html += '<td>'+attachment.name+'</td>';
			}
			html += '<td>'+attachment.size+'KB</td><td>';
			html += '<a href="javascript:void(0);" title="删除" name="'+attachment.name+'" class="btn btn-danger btn-xs js_delete" attachmentId="'+attachment.attachmentId+'">删除</a> ';
			html += '</td></tr>';
		}
		html += '</tbody></table>';
		$('#attachment').prepend(html);
		$('#attachment .js_delete').click(function(){
			var file = $(this);
			bootbox.confirm("是否要删除【"+$(this).attr("name")+"】文件？", function(result) {
				if (result) {
					$.post("${basePath}/user/student/jobLineAttachment/delete.json",{'attachmentId':file.attr("attachmentId")},function(data){
						if(data.result){
							var jobLineId = $('.js_homeWork_update_status').attr('jobLineId');
							getAttachment(jobLineId);
						}
					},"json");
				}
			});		
		});
	}
});
</script>
    

