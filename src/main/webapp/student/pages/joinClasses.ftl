<#assign menu="setting">
<#assign submenu="classCourseManage">
<#include "/commStaticResources/basePage/header.ftl">

<#include "studentMenue.ftl">
 
    
<section id="main-content">
	<section class="wrapper">
		<!-- page start-->
		<div class="row">
			<div class="col-lg-12">
				<!--breadcrumbs start -->
				<ul class="breadcrumb">
					<li><a href="http://localhost:8080/byvision/admin/folder/page.htm?folderId=0"><i class="icon-home"></i>班级课程管理</a></li> 				</ul>
				<!--breadcrumbs end -->
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-5">
				<section class="panel">
					<header class="panel-heading"></header>
					<div class="panel-body">
						 <form id="joinClassesForm" method="post" class="form-horizontal" autocomplete="off" action="${TEMPLATE_PATH}/user/student/joinClasses.json">
							<fieldset>
								
								<div class="form-group">
                                      <label class="col-sm-4 col-sm-4 control-label">班级邀请码</label>
                                      <div class="col-sm-8">
                                          <input type="text" class="form-control" name="classCode" placeholder="邀请码" id="classCode">
                                      </div>
                                  </div>
                                 
                                  <div class="form-group">
                                    <label class="col-sm-2 col-sm-2 control-label"></label>
                                      <button class="btn btn-danger" type="submit">加入班级</button>
                                  </div>		
							</fieldset>
						</form>
					</div>
				</section>
			</div>
			
			<!-- page end-->
		</div>
		
		<div class="row">
			<div class="col-lg-12">
				<section class="panel">
					<header class="panel-heading"> 所属班级列表 </header>
					<div class="panel-body">
						<div class="adv-table">
							<div role="grid" class="dataTables_wrapper" id="hidden-table-info_wrapper">
								<table class="table table-striped table-advance table-hover">
                              <thead>
                                  <tr>
                            <th>班级名称</th>
                            <th>任教课程</th>
                            <th>加入时间</th>
                            <th>操作</th>
                          </tr>
                                </thead>
                              <tbody role="alert" aria-live="polite" aria-relevant="all">
                              
	                           
                                </tbody>
                              </table>
                              <div style="height: 30px;">
                              <div class="pagination"><ul class="pagination"><li class="active"><a href="?p=1" class="number current" title="1">1</a></li><li><a href="?p=2" class="number" title="2">2</a></li><li><a href="?p=2" title="下一页">&gt;</a></li><li><a href="?p=2" title="末页">&gt;&gt;</a></li></ul></div>
                              </div>
                           </div>
						</div>
					</div>
				</section>
			</div>
		</div>
    </section>
</section>

	<#include "studentFooter.ftl">
	
	<script type="text/javascript">
	$(function() {
		$('#joinClassesForm').ajaxForm({
			dataType : 'json',
			success : function(data) {
				if (data.result) {
					bootbox.alert("保存成功，将刷新页面", function() {
						window.location.reload();
					});
				}else{
					showErrors($('#joinClassesForm'),data.errors);
				}
			}
		});
	});	
</script>
