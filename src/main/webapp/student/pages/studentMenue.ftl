<aside>
			<div id="sidebar" class="nav-collapse " tabindex="5000" style="overflow: hidden; outline: none;">
				<!-- sidebar menu goes here-->
				<ul class="sidebar-menu" id="nav-accordion">
					<li class="">
						<a href="${TEMPLATE_PATH}/user/student/index.htm" <#if menu="studentHomeworkIndex">class="active"</#if>> <i class="icon-home"></i> <span>我的作业本</span></a>
					</li>
					
					<li class="sub-menu">
						<a href="javascript:;" <#if menu="setting">class="active"</#if> > <i class="icon-cogs"></i> <span>设置</span><span class="dcjq-icon"></span></a>
						<ul class="sub">
							<li  <#if submenu="classCourseManage">class="active"</#if> ><a href="${TEMPLATE_PATH}/user/student/joinClassPage.htm" >我的班级</a></li>
							<li><a href="${TEMPLATE_PATH}/user/student/profile.htm">修改个人信息</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</aside>