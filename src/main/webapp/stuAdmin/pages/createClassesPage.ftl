<!--header.ftl-->
<#include "/commStaticResources/basePage/header.ftl">

<div class="main container">
		<div class="data_list ">
			<div class="data_list_title">创建班级--以下各项均为必填项</div>
			
			<form method="post" id="createClassForm" action="${TEMPLATE_PATH}/stuAdmin/createClass.htm" class="form-horizontal" >
				<fieldset>
					<!--<div class="form-group">
							<label class="col-lg-3 control-label">学号：</label>
					<div class="col-lg-5">
							<input type="text" class="form-control" name="stuNumber" 
						placeholder="仅供老师查看" >
						</div>
					</div>-->

				
					

					<div class="form-group">
						<label class="col-lg-3 control-label">省份：</label>
						<div class="col-lg-5">
							<select class="form-control" name="provinceId"  id="provinceId" >
								<option value="">-- 选择省份 --</option>
								
							</select>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-lg-3 control-label">学校：</label>
						<div class="col-lg-5">
							<select class="form-control" name="universityId" id="universityId">
								<option value="">-- 选择学校 --</option>
							</select>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-lg-3 control-label">院系：</label>
						<div class="col-lg-5">
							<select class="form-control" name="collegeId" id="collegeId">
								<option value="">-- 选择院系 --</option>
							</select>
						</div>
					</div>
					
					
					<div class="form-group">
						<label class="col-lg-3 control-label">入学年份：</label>
						<div class="col-lg-5">
							<select class="form-control" name="grade" id="grade">
								<option value="">-- 选择入学年份 --</option>
								<option value="2016">2016</option>
								<option value="2015">2015</option>
								<option value="2014">2014</option>
								<option value="2013">2013</option>
								<option value="2012">2012</option>
								<option value="2011">2011</option>
								
							</select>
						</div>
					</div>

					<div class="form-group">
						<label class="col-lg-3 control-label">专业名称：</label>
						<div class="col-lg-5">
							<input type="text" class="form-control" name="subjectName" id="subjectName" placeholder="信息管理与信息系统" >
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-lg-3 control-label">班别：</label>
						<div class="col-lg-5">
							<input type="text" class="form-control" id="classNumber" name="classNumber" placeholder="03" >
						</div>
					</div>
					
					<div class="col-lg-9 col-lg-offset-3">
						<button type="submit" class="btn btn-primary">创建班级</button>
					</div>

				</fieldset>

			</form>
		</div>
	</div>

<!--footer.ftl-->
<#include "/commStaticResources/basePage/footer.ftl">

<script src="${TEMPLATE_PATH}/stuAdmin/js/baseInfoData.js"></script> 
<script src="${TEMPLATE_PATH}/stuAdmin/js/createClassPage.js"></script> 