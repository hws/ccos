var actions = {
	"dept" : "dept",
	"subject":"subject",
	"grade":"grade",
	"class":"class",
	"province":"province",
	"university":"university",
	"college":"college"
};


//initial
$(function() {
	initProvinceData();
	ajaxCascade("provinceId");
	ajaxCascade("universityId");
});



function ajaxCascade(eleId){//ele "#dept" 元素id
	
	$("#"+eleId).change(function(){
		var provinceId = $("#provinceId").val();
		var universityId = $("#universityId").val();
		if(eleId === 'provinceId'){
			ajaxQueryUniversityData(provinceId);
		}else if(eleId === 'universityId'){
			ajaxQueryCollegeData(universityId);
		}
		//ajaxForClassInfo(eleId,provinceId,universityId)
	});
}
function ajaxQueryUniversityData(provinceId){
	$.ajax({
			type: "get",
			dataType: "json",
			url: ctx+"/admin/getUniversityList.json",
			data: {
				provinceId:provinceId
			},
			complete :function(){
			},
			success: function(data){
				var statusHtml ="",id,name,flag;
				
				if(data != undefined){
					$.each(data,function(index,item){
						 id = item.UNIVERSITY_ID;
						 name=item.UNIVERSITY;
						 flag = "universityId";
						 statusHtml+="<option value='"+item.university_id+"_"+item.university+"'>"+item.university+"</option>";
					});
					$("#universityId").empty();//清除之前的记录
					$("<option selected value=''>请选择</option>").appendTo("#universityId");
					$(statusHtml).appendTo("#universityId");
				};
			}
		});
}

function ajaxQueryCollegeData(universityId){
	$.ajax({
			type: "get",
			dataType: "json",
			url: ctx+"/admin/getCollegeList.json",
			data: {
				universityId:universityId
			},
			complete :function(){
			},
			success: function(data){
				var statusHtml ="",id,name,flag;
				
				if(data != undefined){
					$.each(data,function(index,item){
						 id = item.UNIVERSITY_ID;
						 name=item.UNIVERSITY;
						 flag = "universityId";
						 statusHtml+="<option value='"+item.college_id+"_"+item.college+"'>"+item.college+"</option>";
					});
					$("#collegeId").empty();//清除之前的记录
					$("<option selected value=''>请选择</option>").appendTo("#collegeId");
					$(statusHtml).appendTo("#collegeId");
				};
			}
		});
}



function ajaxForClassInfo(eleId,provinceId,universityId){
	$.ajax({
			type: "get",
			dataType: "json",
			url: ctx+"/register/queryBaseData.do",
			data: {
				//action:eleId,deptId:deptId,classNo:classNo,subjectId:subjectId,grade:grade//action表示選擇什麼表，id選擇的id值
				action:eleId,provinceId:provinceId,universityId:universityId//action表示選擇什麼表，id選擇的id值
			},
			complete :function(){
			},
			success: function(data){
				var statusHtml ="",id,name,flag;
				
				if(data != undefined){
					$.each(data,function(index,item){
						if(eleId === "province"){
							 id = item.UNIVERSITY_ID;
							 name=item.UNIVERSITY;
							 flag = "university";
						}else if(eleId === "university"){
							 id = item.COLLEGE_ID;
							 name = item.COLLEGE;
							 flag = "college";
						}
						statusHtml+="<option value='"+id+"_"+name+"'>"+name+"</option>";
					});
					$("#"+actions[flag]).empty();//清除之前的记录
					$("<option selected value=''>请选择</option>").appendTo("#"+actions[flag]);
					$(statusHtml).appendTo("#"+actions[flag]);
				};
			}
		});
}



/*查询出所有的省份基础数据*/
function initProvinceData(){
	
	$.ajax({
		type: "GET",
		dataType: "json",
		url: ctx+"/admin/getProvinceList.json",
		data: {
			//
		},
		complete :function(){
		},
		success: function(data){
			var statusHtml ="";
			if(data != undefined){
				$.each(data,function(index,item){
					statusHtml+="<option value='"+item.province_id+"_"+item.province+"'>"+item.province+"</option>";
				});
				$(statusHtml).appendTo("#provinceId");
			};
		}
	});
}



 