$(document).ready(function() {
    $('#createClassForm').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
        	stuNumber: {
               // message: 'The username is not valid',
                validators: {
                    notEmpty: {
                        message: '用户名不能为空'
                    }
                }
            },
           
            provinceId: {
                validators: {
                    notEmpty: {
                    	message : '省份不能为空!'
                    }
                }
            },
            universityId : {
				validators : {
					notEmpty : {
						message : '学校不能为空!'
					}
				}
			},
			collegeId : {
				validators : {
					notEmpty : {
						message : '院系不能为空！'
					}
				}
			},
			grade : {
				validators : {
					notEmpty : {
						message : '入学年份不能为空！'
					}
				}
			},
			subjectName : {
				validators : {
					notEmpty : {
						message : '专业名称不能为空！'
					}
				}
			},
            classNumber: {
                validators: {
                	notEmpty : {
						message : '班别不能为空！'
					},
					greaterThan : {
						value : 0,
						inclusive : true,
						message : '班别只能大于0'
					},
					regexp : {
						regexp : /^[Z0-9]/,
						message : '只能输入数字'
					}
                }
            }
        }
    });
});



