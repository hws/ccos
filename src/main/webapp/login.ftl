<#include "/commStaticResources/basePage/header.ftl">

	<!--login form-->
	<div class="main container  loginform">
		<div class="data_list user">
			<div class="data_list_title">欢迎登录作业本123</div>
			<form method="post" id="user" action="${TEMPLATE_PATH}/auth/login.json">
				<div class="margin_top10">
					<span class="error"></span>
				</div>
				<div class="margin_top10">
					邮&nbsp;&nbsp;&nbsp;箱:<input type="text" name="loginName"
						id="account"  data_name="邮箱"
						placeholder="邮箱" value="" />
				</div>
				<div class="margin_top10">
					密&nbsp;&nbsp;&nbsp;码:<input type="text" name="password"
						id="password" data_name="密码" placeholder="密码" value=""  />

				</div>
				<div class="margin_top10">
					角&nbsp;&nbsp;&nbsp;色: <span class="input-group-addon"> 老师 <input
						type="radio" value="1"
						name="identity"/ >
					</span> <span class="input-group-addon"> 学生 <input type="radio"
						value="2" name="identity" />
					</span>
				</div>
				

				<div class="margin_top10">
					<p class="input-group-addon">
						<input type="checkbox" id="rememberme" name="rememberme" /> <label for="rememberme" class="span">记住我</label>
						<a class="span2" href="#">忘记密码？</a>
					</p>
					<br>
				</div>
				<div class="margin_top10 post" id="login">
					<input class="span2" type="submit" value="登录" />
				</div>
			</form>
			
			
		</div>

		<br clear="all" />
	</div>
	</div>
	<!--end login form-->

<#include "/commStaticResources/basePage/footer.ftl">