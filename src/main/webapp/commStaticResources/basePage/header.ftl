<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="${SYS_SITEDESC}">
    <meta name="keyword" content="Java, CMS, Spring, MyBatis, MySQL">

    <title>${SYS_SITENAME}</title>

 	<!--Bootstrap core CSS -->
 	<link href="${TEMPLATE_PATH}/commStaticResources/baseFrameWorkUI/bootstrap-3.0.3-dist/dist/css/bootstrap.min.css" rel="stylesheet">
 	<link href="${TEMPLATE_PATH}/commStaticResources/baseFrameWorkUI/bootstrapValidator/css/bootstrapValidator.min.css" rel="stylesheet">
 	<link href="${TEMPLATE_PATH}/commStaticResources/baseCss/bootstrap-datetimepicker.min.css" rel="stylesheet">
 	<link href="${TEMPLATE_PATH}/commStaticResources/baseFrameWorkUI/font-awesome/css/font-awesome.css" rel="stylesheet" />
  	<link href="${TEMPLATE_PATH}/commStaticResources/baseCss/ccos.css" rel="stylesheet">
  	<link href="${TEMPLATE_PATH}/commStaticResources/baseJs/uploadify/uploadify.css" rel="stylesheet">
  	
  	<link href="${TEMPLATE_PATH}/commStaticResources/baseCss/list.css" rel="stylesheet">
  	<!--<link href="${TEMPLATE_PATH}/commStaticResources/baseCss/manhuaDate.1.0.css" rel="stylesheet">-->
  	
  	
  	<script type="text/javascript">
	var ctx = "${TEMPLATE_PATH}";
</script>
  </head>
  <body>
  
  <div class="header">
		<div class="container">
			<div class="row-fluid nav_wrap">
				<ul class="nav nav-pills">
					<li class="active"><a href="${TEMPLATE_PATH}/index.htm">首页</a></li>
					<li><a href="#">分享</a></li>
					<li><a href="#">话题</a></li>
					<li><a href="#">动态</a></li>
					<li><a href="${TEMPLATE_PATH}/auth/login.htm">登陆</a></li>
					<li><a href="${TEMPLATE_PATH}/auth/register.htm">注册</a></li>
				</ul>
			</div>
		</div>
	</div>
  	