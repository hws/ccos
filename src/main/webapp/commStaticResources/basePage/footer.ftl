<div class="footer margin_top10">
		<div class="container">
			<a href="#">帮助</a> <span>|</span> <a href="#">联系我</a> <span>|</span>
			<a href="#">关于作业本团队</a> <span>|</span> <span class="pull-center">
				粤ICP备案111111111号 </span>
		</div>
	</div>
	
	 <!-- js placed at the end of the document so the pages load faster -->
    <script src="${TEMPLATE_PATH}/commStaticResources/baseJs/jquery-1.10.2.min.js"></script>
    <script src="${TEMPLATE_PATH}/commStaticResources/baseFrameWorkUI/bootstrap-3.0.3-dist/dist/js/bootstrap.min.js"></script>
    <script src="${TEMPLATE_PATH}/commStaticResources/baseFrameWorkUI/bootstrapValidator/js/bootstrapValidator.min.js"></script>
    <!--common script for all pages-->
    <script src="${TEMPLATE_PATH}/commStaticResources/baseJs/jquery.form.min.js"></script> 
    <!--left menu-->
    <script src="${TEMPLATE_PATH}/commStaticResources/baseJs/jquery.dcjqaccordion.2.7.js"></script> 
    <script src="${TEMPLATE_PATH}/commStaticResources/baseJs/bootbox.min.js"></script> 
    
    <script src="${TEMPLATE_PATH}/commStaticResources/baseJs/ui.tab.js"></script> 
    <script src="${TEMPLATE_PATH}/commStaticResources/baseJs/shaixuan.js"></script> 
    
</body>
</html>

