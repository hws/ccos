<!--header.ftl-->
<#include "/commStaticResources/basePage/header.ftl">

	<div class="main container">
		<div class="data_list ">
			<div class="data_list_title">欢迎加入作业本   以下各项均为必填项</div>
			
			<form  id="registerForm"  class="form-horizontal" action="${TEMPLATE_PATH}/auth/register.json" method="POST">
				<fieldset>
					<div class="form-group">
						<label class="col-lg-3 control-label" for="realName">真实姓名:</label>
					<div class="col-lg-4">
							<input type="text" class="form-control"  name="realName" id="realName" data_name="真实姓名"
						placeholder="如：李易峰" >
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-lg-3 control-label" for="email">邮箱:</label>
						<div class="col-lg-4">
							<input type="text" class="form-control" id="email" name="email"   >
						</div>
					</div>
					
				<div class="form-group">
						<label class="col-lg-3 control-label" for="password">密码:</label>
						<div class="col-lg-4">
							<input type="password" class="form-control" id="password" name="password"   >
						</div>
					</div>
					
					
					<div class="form-group">
						<label class="col-lg-3 control-label" for="confirmPassword">确认密码:</label>
						<div class="col-lg-4">
							<input type="password" class="form-control" id="comfirmPassword" name="comfirmPassword"   >
						</div>
					</div>

					<!--<div class="form-group">
						<label class="col-lg-3 control-label" for="gender">性别:</label>
						<div class="col-lg-4">
							<label class="radio"> 
								<input type="radio" id="optionsCheckbox" value="MALE" name="gender"> 男
							</label> 
							
							<label class="radio"> 
								 女<input type="radio" id="optionsCheckbox1" value="FEMALE" name="gender"> 
							</label> 
						</div>
					</div>-->
					
					<div class="form-group">
						<label class="col-lg-3 control-label" for="gender">角色:</label>
						<div class="col-lg-4">
							<label class="radio"> 
								<input type="radio" id="optionsCheckbox" value="STUDENT" name="roleType"> 普通学生
							</label> 
							
							<label class="radio"> 
								 学习委员（管理班级）<input type="radio" id="optionsCheckbox1" value="STUDENTADMIN" name="roleType"> 
							</label> 
							
							<label class="radio"> 
								 老师<input type="radio" id="optionsCheckbox1" value="TEACHER" name="roleType"> 
							</label> 
						</div>
					</div>
					

					<div class="form-group">
						<label class="col-lg-3 control-label" for="repwd">验证码:</label>
						<div class="col-lg-4">
							<label class=""> <input type="text"
								 id="captcha" name="captcha" maxLength="4"
								size="10" style="width: 60px;" /> <img id="randomImg"
								src="${TEMPLATE_PATH}/servlet/ticketImage" />
								<a href="javascript:changeImg();">换一张</a>
							</label>
						</div>
					</div>

					<div class="col-lg-9 col-lg-offset-3">
						<button type="submit" class="btn btn-primary">注册</button>
					</div>

				</fieldset>

			</form>
		</div>
	</div>


<!--footer.ftl-->
<#include "/commStaticResources/basePage/footer.ftl">

<script src="${TEMPLATE_PATH}/user/js/registerValidate.js"></script> 

<script type="text/javascript">
	$(function() {
		$('#registerForm').ajaxForm({
			dataType : 'json',
			success : function(data) {
				if (data.result) {
					bootbox.alert("注册成功", function() {
						location.href= ctx+ '/auth/registerSuccess.htm';			
					});
				}else{
					//showErrors($('#registerForm'),data.errors);
				}
			}
		});
	});	
</script>

