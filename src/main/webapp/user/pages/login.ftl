<#include "/commStaticResources/basePage/header.ftl">


	<!--login form-->
	<div class="main container  loginform">
		<div class="data_list user">
			<div class="data_list_title">欢迎登录作业本</div>
			</br>
			
			<form  id="loginForm"  class="form-horizontal" action="${basePath}/auth/login.json" method="post" >
				<fieldset>
					
					<div class="form-group">
						<label class="col-lg-3 control-label" for="email">邮箱:</label>
						<div class="col-lg-4">
							<input type="text" class="form-control" id="email" name="email"   >
						</div>
					</div>
					
				<div class="form-group">
						<label class="col-lg-3 control-label" for="password">密码:</label>
						<div class="col-lg-4">
							<input type="password" class="form-control" id="password" name="password"   >
						</div>
					</div>
					

					<div class="form-group">
						<label class="col-lg-3 control-label" for="gender">角色:</label>
						<div class="col-lg-4">
							<label class="radio"> 
								<input type="radio" id="optionsCheckbox" value="STUDENT" name="roleType"> 普通学生
							</label> 
							
							<label class="radio"> 
								 学习委员<input type="radio" id="optionsCheckbox1" value="STUDENTADMIN" name="roleType"> 
							</label> 
							
							<label class="radio"> 
								 老师<input type="radio" id="optionsCheckbox1" value="TEACHER" name="roleType"> 
							</label> 
						</div>
					</div>
					

					<div class="col-lg-9 col-lg-offset-3">
					<button  class="btn btn-primary" id="loginButton" >登录</button>
					</div>

				</fieldset>

			</form>
		</div>

		<br clear="all" />
	</div>
	</div>
	<!--end login form-->
<!--footer.ftl-->
<#include "/commStaticResources/basePage/footer.ftl">
<script type="text/javascript">
/**
 * 显示表单的错误提示
 * @param id 表单ID
 * @param errors 错误列表
 */
function showErrors(id,errors){
	id.find('p[class=error]').hide();
	id.find('input,select').removeClass("error");
	for(var name in errors){
		if(errors.roleType == undefined){
			var e = id.find('p[for='+name+']');
			id.find('input[name='+name+'],select[name='+name+']').addClass("error");
		}else{
			var e = id.find('p[for='+name+']');
			id.find('input[name=roleType]:checked').addClass("error");
		}
		if(e.length==0){
			if(errors.roleType == undefined){
				id.find('input[name='+name+'],select[name='+name+']').after('<p for="'+name+'" class="error"></p>');
			}else{
				id.find('input[value=TEACHER]').after('<p for="'+name+'" class="error"></p>');
			}
			e = id.find('p[for='+name+']');
		}
		if(errors[name]!=""){
			e.html(errors[name]);
			e.show();
		}
	}
}
	$(function() {
		
		//var user={"email":$('#email').val(),"password":$('#password').val(),"roleType":$('input[name=roleType]:checked').val()};  
		$('#loginForm').ajaxForm({
			dataType : 'json',
			success : function(data) {
				if (data.result) {
					var role = $('input[name=roleType]:checked').val();
					var url = '';
					if(role === 'STUDENT'){
						url= ctx + '/user/student/index.htm';			
					}else if(role === 'STUDENTADMIN'){
						url=ctx + '/stuAdmin/index.htm';			
					}else if(role === 'TEACHER'){
						url=ctx + '/user/teacher/index.htm';			
					}
					location.href= url;
					
					console.log(url);
				}else{
					showErrors($('#loginForm'),data.errors);
				}
			}
		});
	});	
</script> 
   
