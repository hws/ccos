<!--header.ftl-->
<#include "/commStaticResources/basePage/header.ftl">

	<div class="main container">
		<div class="data_list ">
			<div class="data_list_title">欢迎加入作业本   以下各项均为必填项</div>
			
			<form  id="registerForm"  class="form-horizontal" action="${TEMPLATE_PATH}/auth/register.json" method="POST">
				<fieldset>
					<div class="form-group">
						<label class="col-lg-3 control-label" for="realName">真实姓名:</label>
					<div class="col-lg-4">
							<input type="text" class="form-control"  name="realName" id="realName" data_name="真实姓名"
						placeholder="如：李易峰" >
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-lg-3 control-label" for="email">邮箱:</label>
						<div class="col-lg-4">
							<input type="text" class="form-control" id="email" name="email"   >
						</div>
					</div>
					
				<div class="form-group">
						<label class="col-lg-3 control-label" for="password">密码:</label>
						<div class="col-lg-4">
							<input type="password" class="form-control" id="password" name="password"   >
						</div>
					</div>
					
					
					<div class="form-group">
						<label class="col-lg-3 control-label" for="confirmPassword">确认密码:</label>
						<div class="col-lg-4">
							<input type="password" class="form-control" id="comfirmPassword" name="comfirmPassword"   >
						</div>
					</div>

					<!--<div class="form-group">
						<label class="col-lg-3 control-label" for="gender">性别:</label>
						<div class="col-lg-4">
							<label class="radio"> 
								<input type="radio" id="optionsCheckbox" value="MALE" name="gender"> 男
							</label> 
							
							<label class="radio"> 
								 女<input type="radio" id="optionsCheckbox1" value="FEMALE" name="gender"> 
							</label> 
						</div>
					</div>-->
					
					<div class="form-group">
						<label class="col-lg-3 control-label" for="gender">角色:</label>
						<div class="col-lg-4">
							<label class="radio"> 
								<input type="radio" id="optionsCheckbox" value="STUDENT" name="roleType"> 普通学生
							</label> 
							
							<label class="radio"> 
								 学习委员（管理班级）<input type="radio" id="optionsCheckbox1" value="STUDENTADMIN" name="roleType"> 
							</label> 
							
							<label class="radio"> 
								 老师<input type="radio" id="optionsCheckbox1" value="TEACHER" name="roleType"> 
							</label> 
						</div>
					</div>
					

					<div class="form-group">
						<label class="col-lg-3 control-label" for="repwd">验证码:</label>
						<div class="col-lg-4">
							<label class=""> <input type="text"
								 id="captcha" name="captcha" maxLength="4"
								size="10" style="width: 60px;" /> <img id="randomImg"
								src="${TEMPLATE_PATH}/servlet/ticketImage" />
								<a href="javascript:changeImg();">换一张</a>
							</label>
						</div>
					</div>

					<div class="col-lg-9 col-lg-offset-3">
						<button type="submit" class="btn btn-primary">注册</button>
					</div>

				</fieldset>

			</form>
		</div>
	</div>


<!--footer.ftl-->
<#include "/commStaticResources/basePage/footer.ftl">



<script type="text/javascript">
	function changeImg() {
    	$("#randomImg").attr("src", ctx+'/servlet/ticketImage?nocache=' + new Date().getTime());
 	}

	$(function() {
			//ajaxRegister();
	
	changeImg();
	//ajaxRegister();
	
	$('#registerForm').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
        	realName: {
                validators: {
                    notEmpty: {
                    	message:'真实姓名不能为空!'
                    },
                    stringLength: {
                        min: 2,
                        max: 10,
                        message:'2~10个汉字，必须为汉字，一旦注册成功真实姓名不能修改。'
                    }
                }
            },
            email: {
            	 verbose: false,
            	validators: {
                    notEmpty: {
                        message: '邮箱不能为空!'
                    },
                    emailAddress: {
                        message: '您填写的邮箱格式不正确!'
                    },
                    remote : {
						message: '改邮箱已经被注册！',
						url: ctx+'/auth/validateAuth.htm?checkType=emailType',
						//url: ctx+'/test1.do',
						type : 'GET',
						data : {
							inputValue : function() {
								return email = $('[name="email"]').val();
							}
						},
						 dataType: 'json',
					}
                }
            },
            password: {
            	message:'6~16个字符，为了您的帐号安全，请使用字母加数字或符号的组合密码！',
                validators: {
                    notEmpty: {
                        message: '密码不能为空!'
                    },
                    identical: {
                        field: 'confirmPassword',
                        message: '密码与确认密码不一致，请重新输入!'
                    }
                }
            },
            comfirmPassword: {
                validators: {
                    notEmpty: {
                        message: '确认密码不能为空!'
                    },
                    identical: {
                        field: 'password',
                        message: '确认密码与密码不一致，请重新输入!'
                    }
                }
            },
            
            roleType: {
                validators: {
                    notEmpty: {
                        message: '角色不能为空！'
                    }
                }
            },
            gender: {
                validators: {
                    notEmpty: {
                        message: '性别不能为空！'
                    }
                }
            },
            captcha:{
            	 validators: {
                     notEmpty: {
                         message: '验证码不能为空！'
                     },
                     remote : {
 						message: '验证码不对，请重新输入！',
 						url: ctx+'/auth/validateAuth.htm?checkType=validatorCode',
 						type : 'GET',
 						data : {
 							inputValue : function() {
 								return email = $('[name="captcha"]').val();
 							}
 						},
 						 dataType: 'json',
 					}
                 }
            },
            acceptTerms: {
                validators: {
                    notEmpty: {
                        message: 'You have to accept the terms and policies'
                    }
                }
            }
        }
    });
	
		$('#registerForm').ajaxForm({
			dataType : 'json',
			success : function(data) {
				if (data.result) {
					bootbox.alert("注册成功", function() {
						location.href= ctx+ '/auth/registerSuccess.htm';			
					});
				}else{
					//showErrors($('#registerForm'),data.errors);
				}
			}
		});
	});	
	
	
	
</script>

