<#include "/commStaticResources/basePage/header.ftl">

<link href="${TEMPLATE_PATH}/commStaticResources/baseCss/index.css" rel="stylesheet">
<div class="site-content site-content-without-padding">

    

    <div class="content-sections">

    <div class="content-section-a" id="about">
        <div class="container">
            <div class="frontpage-section-about">
                <h2 class="section-heading">关于高校校园管家系统</h2>
                <p class="lead">
                   	方便，快捷，高效的校园办公系统。线上和线下资源整合，实现学习生活资源最大化。
                </p>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <h3>作业管理模块 </h3>
                    <p>老师发布作业，学生提交，老师批改，给师生提供一个高效便捷的管理作业平台。</p>
                </div>
                <div class="col-sm-4">
                    <h3>社团管理</h3>
                    <p>实现校内校外高校的社团资源分享，让更多学生和赞助商可以参与其中.</p>
                </div>
                <div class="col-sm-4">
                    <h3>其他校园生活方面的管理</h3>
                    <p>校内外的社交化。</p>
                </div>
            </div>

            <div style="text-align: center; margin-top: 2em">
                <a href="https://www.phusionpassenger.com/#why">
                    <span class="glyphicon glyphicon-book" aria-hidden="true"></span>
                    了解更多
                </a>
            </div>
        </div><!-- /.container -->
    </div><!-- /.content-section-a -->
</div>
    

   

    




    </div><!-- /.content-sections -->
</div>


<#include "/commStaticResources/basePage/footer.ftl">
