<!--header.ftl-->
<#include "/commStaticResources/basePage/header.ftl">

	<div class="main container">
		<div class="data_list ">
			<div class="data_list_title">注册成功</div>
			
			恭喜  ${SESSION_USER.email} 用户注册成功
			<br>
			<br>
			<#if SESSION_USER.roleType  == "STUDENTADMIN">
				<form action="${TEMPLATE_PATH}/stuAdmin/createClassPage.htm" method="GET">
			<#else>	
				<form action="profilePage.htm" method="GET">
			</#if>
				<#if SESSION_USER.roleType  == "STUDENTADMIN">
					<input type="hidden" value = "${SESSION_USER.roleType }" name="roleType">
					<button type="submit" class="btn btn-primary">创建班级</button>
				<#else>
					<input type="hidden" value = "${SESSION_USER.roleType }" name="roleType">
					<button type="submit" class="btn btn-primary">完善个人信息</button>
				</#if>
			</form>
			<br>
			<form action="gotoMainPage.do" method="GET">
				<!-- <a href="#">暂不完善</a> -->
				<button type="submit" class="btn btn-primary">我的主页</button>
			</form>
			<input type="hidden" value = "${SESSION_USER.roleType }" name="roleType">
		</div>
	</div>

	<!--footer.ftl-->
<#include "/commStaticResources/basePage/footer.ftl">