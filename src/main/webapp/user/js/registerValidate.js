$(document).ready(function() {
	//ajaxRegister();
	
	changeImg();
	//ajaxRegister();
	
	$('#registerForm').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
        	realName: {
                validators: {
                    notEmpty: {
                    	message:'真实姓名不能为空!'
                    },
                    stringLength: {
                        min: 2,
                        max: 10,
                        message:'2~10个汉字，必须为汉字，一旦注册成功真实姓名不能修改。'
                    }
                }
            },
            email: {
            	 verbose: false,
            	validators: {
                    notEmpty: {
                        message: '邮箱不能为空!'
                    },
                    emailAddress: {
                        message: '您填写的邮箱格式不正确!'
                    },
                    remote : {
						message: '改邮箱已经被注册！',
						url: ctx+'/auth/validateAuth.htm?checkType=emailType',
						//url: ctx+'/test1.do',
						type : 'GET',
						data : {
							inputValue : function() {
								return email = $('[name="email"]').val();
							}
						},
						 dataType: 'json',
					}
                }
            },
            password: {
            	message:'6~16个字符，为了您的帐号安全，请使用字母加数字或符号的组合密码！',
                validators: {
                    notEmpty: {
                        message: '密码不能为空!'
                    },
                    identical: {
                        field: 'confirmPassword',
                        message: '密码与确认密码不一致，请重新输入!'
                    }
                }
            },
            comfirmPassword: {
                validators: {
                    notEmpty: {
                        message: '确认密码不能为空!'
                    },
                    identical: {
                        field: 'password',
                        message: '确认密码与密码不一致，请重新输入!'
                    }
                }
            },
            
            roleType: {
                validators: {
                    notEmpty: {
                        message: '角色不能为空！'
                    }
                }
            },
            gender: {
                validators: {
                    notEmpty: {
                        message: '性别不能为空！'
                    }
                }
            },
            captcha:{
            	 validators: {
                     notEmpty: {
                         message: '验证码不能为空！'
                     },
                     remote : {
 						message: '验证码不对，请重新输入！',
 						url: ctx+'/auth/validateAuth.htm?checkType=validatorCode',
 						type : 'GET',
 						data : {
 							inputValue : function() {
 								return email = $('[name="captcha"]').val();
 							}
 						},
 						 dataType: 'json',
 					}
                 }
            },
            acceptTerms: {
                validators: {
                    notEmpty: {
                        message: 'You have to accept the terms and policies'
                    }
                }
            }
        }
    });
});


function changeImg() {
    $("#randomImg").attr("src", ctx+'/servlet/ticketImage?nocache=' + new Date().getTime());
 }

function ajaxRegister(){
	
	/* var dataArray=[];  
    var user={"realName":$('#realName').val(),"email":$('#email').val(),"password":$('#password').val(),"comfirmPassword":$('#comfirmPassword').val(),"roleType":$('input[name=roleType]:checked').val(),"captcha":$('#captcha').val()};  
    dataArray.push(user);
	 $.ajax({ 
         type:"POST", 
         url:ctx+'/auth/register.json', 
         dataType:"json",      
         contentType:"application/json",               
         data:JSON.stringify(user), //$('#registerForm').serialize(), 
         success:function(data){ 
                //console.log(data);
        	 location.href= ctx+ '/auth/registerSuccess.htm';			
         } 
      }); */
	
	
	$('#registerForm').ajaxForm({
		dataType : 'json',
		success : function(data) {
			if (data.result) {
				bootbox.alert("发布成功，将刷新页面", function() {
					window.location.reload();
				});
			}else{
				showErrors($('#releaseHomeWorkForm'),data.errors);
			}
		}
	});
}