/**
 * 显示表单的错误提示
 * @param id 表单ID
 * @param errors 错误列表
 */
function showErrors(id,errors){
	id.find('p[class=error]').hide();
	id.find('input,select').removeClass("error");
	for(var name in errors){
		if(errors.roleType == undefined){
			var e = id.find('p[for='+name+']');
			id.find('input[name='+name+'],select[name='+name+']').addClass("error");
		}else{
			var e = id.find('p[for='+name+']');
			id.find('input[name=roleType]:checked').addClass("error");
		}
		if(e.length==0){
			if(errors.roleType == undefined){
				id.find('input[name='+name+'],select[name='+name+']').after('<p for="'+name+'" class="error"></p>');
			}else{
				id.find('input[value=TEACHER]').after('<p for="'+name+'" class="error"></p>');
			}
			e = id.find('p[for='+name+']');
		}
		if(errors[name]!=""){
			e.html(errors[name]);
			e.show();
		}
	}
}
	$(function() {
		
		//var user={"email":$('#email').val(),"password":$('#password').val(),"roleType":$('input[name=roleType]:checked').val()};  
		$('#loginForm').ajaxForm({
			dataType : 'json',
			success : function(data) {
				if (data.result) {
					var role = $('input[name=roleType]:checked').val();
					var url = '';
					if(role === 'STUDENT'){
						url= ctx + '/user/student/index.htm';			
					}else if(role === 'STUDENTADMIN'){
						url=ctx + '/stuAdmin/index.htm';			
					}else if(role === 'TEACHER'){
						url=ctx + '/user/teacher/index.htm';			
					}
					location.href= url;
					
					console.log(url);
				}else{
					showErrors($('#loginForm'),data.errors);
				}
			}
		});
	});	
